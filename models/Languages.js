module.exports = (db, DataTypes) => {
    const language = db.define('language', {
        id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true, allowNull: false },
        iso6392: { type: DataTypes.STRING(50), allowNull: true },
        iso6395: { type: DataTypes.STRING(50), allowNull: true },
        iso6391: { type: DataTypes.STRING(50), allowNull: true },
        name: { type: DataTypes.STRING(500), allowNull: false },
        native: { type: DataTypes.STRING(500), allowNull: true },
        f_active: { type: DataTypes.BOOLEAN, allowNull: true },
    });
    return language;
}