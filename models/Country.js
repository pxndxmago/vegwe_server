module.exports = (db, DataTypes) => {
    const country = db.define('country', {
        id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true, allowNull: false },
        name: { type: DataTypes.STRING(500) },
        iso3: { type: DataTypes.STRING(500) },
        iso2: { type: DataTypes.STRING(500) },
        phonecode: { type: DataTypes.STRING(500) },
        capital: { type: DataTypes.STRING(500) },
        currency: { type: DataTypes.STRING(500) },
        native: { type: DataTypes.STRING(500) },
        emoji: { type: DataTypes.STRING(500) },
        emojiU: { type: DataTypes.STRING(500) },
        flag: { type: DataTypes.BOOLEAN },
        wikiDataId: { type: DataTypes.STRING(500) },
    });
    return country;
}