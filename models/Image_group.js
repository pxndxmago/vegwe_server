module.exports = (db, DataTypes) => {
    const image_group = db.define('image_group', {
        idImage: { type: DataTypes.INTEGER, primaryKey: true, allowNull: false, autoIncrement: true },
        principal: { type: DataTypes.INTEGER, allowNull: false },
        route: { type: DataTypes.TEXT('long'), allowNull: false },
        thumbnailRoute: { type: DataTypes.TEXT('long'), allowNull: true },
        smallRoute: { type: DataTypes.TEXT('long'), allowNull: true },
        mediumRoute: { type: DataTypes.TEXT('long'), allowNull: true },
        f_active: { type: DataTypes.BOOLEAN, allowNull: false },
    });
    return image_group;
}