module.exports = (db, DataTypes) => {
    const report = db.define('report',
        {
            idReport: { type: DataTypes.INTEGER, primaryKey: true, allowNull: false, autoIncrement: true },
            idUser: { type: DataTypes.INTEGER, allowNull: false },
            idUserReported: { type: DataTypes.INTEGER, allowNull: false },
            idReportReason: { type: DataTypes.INTEGER, allowNull: false }
        }, {
        indexes: [
            {
                unique: true,
                fields: ['idUser', 'idUserReported']
            }
        ]
    });
    return report;
};