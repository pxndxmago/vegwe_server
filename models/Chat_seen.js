module.exports = (db, DataTypes) => {
    const chat_seen = db.define('chat_seen', {
        f_seen: { type: DataTypes.BOOLEAN, allowNull: true }
    });
    return chat_seen;
}