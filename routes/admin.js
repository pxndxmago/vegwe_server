const AdminBro = require('admin-bro')
const AdminBroExpress = require('admin-bro-expressjs')
const AdminBroSequelize = require('admin-bro-sequelizejs')
AdminBro.registerAdapter(AdminBroSequelize)
const db = require('../models');
const express = require('express')
const app = express()

const UserParent = {
    name: 'Users',
    icon: 'user--multiple',
}

const adminBro = new AdminBro({
    databases: [db],
    rootPath: '/admin',
    resources: [
        { resource: db.user, options: { parent: UserParent } },
        { resource: db.city, options: { parent: UserParent } },
        { resource: db.country, options: { parent: UserParent } }
    ]
})

const ADMIN = {
    email: process.env.ADMIN_EMAIL || 'arturo@gmail.com',
    password: process.env.ADMIN_PASSWORD || 'moises23',
}

const router = AdminBroExpress.buildAuthenticatedRouter(adminBro, {
    cookieName: process.env.ADMIN_COOKIE_NAME || 'admin-pro',
    cookiePassword: process.env.ADMIN_COOKIE_PASS || 'super-secret-and-long-password-for-a-cookie-in-the-browser',
    authenticate: async (email, password) => {
        if (ADMIN.email == email && password == ADMIN.password) {
            return ADMIN;
        }
        return null;
    }
})

module.exports = router;