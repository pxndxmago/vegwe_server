module.exports = (db, DataTypes) => {
    const user_location = db.define('user_location', {
        idUserLocation: { type: DataTypes.INTEGER, primaryKey: true, allowNull: false, autoIncrement: true },
        latitude: { type: DataTypes.DOUBLE(10, 8) },
        longitude: { type: DataTypes.DOUBLE(10, 8) },
    });

    return user_location;
};