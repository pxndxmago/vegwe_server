module.exports = (db, DataTypes) => {
    const interest = db.define('interest', {
        idInterest: { type: DataTypes.INTEGER, primaryKey: true, allowNull: false, autoIncrement: true },
        cod: { type: DataTypes.STRING(100), allowNull: false },
        locale: { type: DataTypes.STRING(50), allowNull: false },
        text: { type: DataTypes.STRING(1000), allowNull: false }
    });
    return interest;
}