module.exports = {
    'secret': 'the_master_letsvegan_key',
    'app_port': 3002,
    'chat_port': 3003,
    'expiresIn': "30d"
};