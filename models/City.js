module.exports = (db, DataTypes) => {
    const city = db.define('city', {
        id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement:true, allowNull: false },
        name: { type: DataTypes.STRING(500) },
        state_id: { type: DataTypes.INTEGER },
        state_code: { type: DataTypes.STRING(500) },
        country_id: { type: DataTypes.INTEGER },
        country_code: { type: DataTypes.STRING(500) },
        latitude: { type: DataTypes.DOUBLE(10,8) },
        longitude: { type: DataTypes.DOUBLE(10,8) },
        flag: { type: DataTypes.BOOLEAN },
        wikiDataId: { type: DataTypes.STRING(500) },
    });
    return city;
}