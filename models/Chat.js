module.exports = (db, DataTypes) => {
    const chat = db.define('chat', {
        idChat: { type: DataTypes.INTEGER, primaryKey: true, allowNull: false, autoIncrement: true },
        nameChat: { type: DataTypes.STRING(200), allowNull: false },
        description: { type: DataTypes.STRING(1000), allowNull: false },
        countryId: { type: DataTypes.INTEGER, allowNull: true },
        languages: { type: DataTypes.STRING(500), allowNull: true },
        f_group: { type: DataTypes.BOOLEAN, allowNull: false },
    });
    return chat;
}