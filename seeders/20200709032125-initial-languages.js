'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {}) 
    */
    return queryInterface.bulkInsert('languages', [
      {
        id: "1",
        iso6392: "abk",
        iso6395: "abk",
        iso6391: "ab",
        name: "Abkhazian",
        native: "Аҧсуа бызшәа  Аҧсшәа",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "2",
        iso6392: "ace",
        iso6395: "ace",
        name: "Achinese",
        native: "بهسا اچيه",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "3",
        iso6392: "ach",
        iso6395: "ach",
        name: "Acoli",
        native: "Lwo",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "4",
        iso6392: "ada",
        iso6395: "ada",
        name: "Adangme",
        native: "Dangme",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "5",
        iso6392: "ady",
        iso6395: "ady",
        name: "Adyghe  Adygei",
        native: "Адыгабзэ  Кӏахыбзэ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "6",
        iso6392: "aar",
        iso6395: "aar",
        iso6391: "aa",
        name: "Afar",
        native: "Qafaraf  ’Afar Af  Afaraf  Qafar af",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "7",
        iso6392: "afh",
        iso6395: "afh",
        name: "Afrihili",
        native: "El-Afrihili",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "8",
        iso6392: "afr",
        iso6395: "afr",
        iso6391: "af",
        name: "Afrikaans",
        native: "Afrikaans",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "9",
        iso6392: "afa",
        name: "Afro-Asiatic languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "10",
        iso6392: "ain",
        iso6395: "ain",
        name: "Ainu",
        native: "アイヌ・イタㇰ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "11",
        iso6392: "aka",
        iso6395: "aka",
        iso6391: "ak",
        name: "Akan",
        native: "Akan",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "12",
        iso6392: "akk",
        iso6395: "akk",
        name: "Akkadian",
        native: "𒀝𒅗𒁺𒌑",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "13",
        iso6392: "alb* / sqi",
        iso6395: "sqi",
        iso6391: "sq",
        name: "Albanian",
        native: "Shqip",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "14",
        iso6392: "sqi / alb*",
        iso6395: "sqi",
        iso6391: "sq",
        name: "Albanian",
        native: "Shqip",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "15",
        iso6392: "ale",
        iso6395: "ale",
        name: "Aleut",
        native: "Уна́ӈам тунуу́  Унаӈан умсуу",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "16",
        iso6392: "alg",
        name: "Algonquian languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "17",
        iso6392: "tut",
        name: "Altaic languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "18",
        iso6392: "amh",
        iso6395: "amh",
        iso6391: "am",
        name: "Amharic",
        native: "አማርኛ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "19",
        iso6392: "anp",
        iso6395: "anp",
        name: "Angika",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "20",
        iso6392: "apa",
        name: "Apache languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "21",
        iso6392: "ara",
        iso6395: "ara",
        iso6391: "ar",
        name: "Arabic",
        native: "العَرَبِيَّة",
        f_active: "1",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "22",
        iso6392: "arg",
        iso6395: "arg",
        iso6391: "an",
        name: "Aragonese",
        native: "aragonés",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "23",
        iso6392: "arp",
        iso6395: "arp",
        name: "Arapaho",
        native: "Hinónoʼeitíít",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "24",
        iso6392: "arw",
        iso6395: "arw",
        name: "Arawak",
        native: "Lokono",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "25",
        iso6392: "arm* / hye",
        iso6395: "hye",
        iso6391: "hy",
        name: "Armenian",
        native: "Հայերէն  Հայերեն",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "26",
        iso6392: "hye / arm*",
        iso6395: "hye",
        iso6391: "hy",
        name: "Armenian",
        native: "Հայերէն  Հայերեն",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "27",
        iso6392: "rup",
        iso6395: "rup",
        name: "Aromanian  Arumanian  Macedo-Romanian",
        native: "armãneashce  armãneashti  rrãmãneshti",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "28",
        iso6392: "art",
        name: "Artificial languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "29",
        iso6392: "asm",
        iso6395: "asm",
        iso6391: "as",
        name: "Assamese",
        native: "অসমীয়া",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "30",
        iso6392: "ast",
        iso6395: "ast",
        name: "Asturian  Bable  Leonese  Asturleonese",
        native: "Asturianu",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "31",
        iso6392: "ath",
        name: "Athapascan languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "32",
        iso6392: "aus",
        name: "Australian languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "33",
        iso6392: "map",
        name: "Austronesian languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "34",
        iso6392: "ava",
        iso6395: "ava",
        iso6391: "av",
        name: "Avaric",
        native: "Магӏарул мацӏ  Авар мацӏ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "35",
        iso6392: "ave",
        iso6395: "ave",
        iso6391: "ae",
        name: "Avestan",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "36",
        iso6392: "awa",
        iso6395: "awa",
        name: "Awadhi",
        native: "अवधी",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "37",
        iso6392: "aym",
        iso6395: "aym",
        iso6391: "ay",
        name: "Aymara",
        native: "Aymar aru",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "38",
        iso6392: "aze",
        iso6395: "aze",
        iso6391: "az",
        name: "Azerbaijani",
        native: "Azərbaycan dili  آذربایجان دیلی  Азәрбајҹан дили",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "39",
        iso6392: "ban",
        iso6395: "ban",
        name: "Balinese",
        native: "ᬪᬵᬱᬩᬮᬶ  ᬩᬲᬩᬮᬶ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "40",
        iso6392: "bat",
        name: "Baltic languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "41",
        iso6392: "bal",
        iso6395: "bal",
        name: "Baluchi",
        native: "بلوچی",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "42",
        iso6392: "bam",
        iso6395: "bam",
        iso6391: "bm",
        name: "Bambara",
        native: "ߓߊߡߊߣߊߣߞߊߣ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "43",
        iso6392: "bai",
        name: "Bamileke languages",
        native: "Bamiléké",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "44",
        iso6392: "bad",
        name: "Banda languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "45",
        iso6392: "bnt",
        name: "Bantu (Other)",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "46",
        iso6392: "bas",
        iso6395: "bas",
        name: "Basa",
        native: "Mbene  Ɓasaá",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "47",
        iso6392: "bak",
        iso6395: "bak",
        iso6391: "ba",
        name: "Bashkir",
        native: "Башҡорт теле  Başqort tele",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "48",
        iso6392: "baq* / eus",
        iso6395: "eus",
        iso6391: "eu",
        name: "Basque",
        native: "euskara",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "49",
        iso6392: "eus / baq*",
        iso6395: "eus",
        iso6391: "eu",
        name: "Basque",
        native: "euskara",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "50",
        iso6392: "btk",
        name: "Batak languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "51",
        iso6392: "bej",
        iso6395: "bej",
        name: "Beja  Bedawiyet",
        native: "Bidhaawyeet",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "52",
        iso6392: "bel",
        iso6395: "bel",
        iso6391: "be",
        name: "Belarusian",
        native: "Беларуская мова",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "53",
        iso6392: "bem",
        iso6395: "bem",
        name: "Bemba",
        native: "Chibemba",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "54",
        iso6392: "ben",
        iso6395: "ben",
        iso6391: "bn",
        name: "Bengali",
        native: "বাংলা",
        f_active: "1",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "55",
        iso6392: "ber",
        name: "Berber languages",
        native: "Tamaziɣt  Tamazight  ⵜⴰⵎⴰⵣⵉⵖⵜ  ⵝⴰⵎⴰⵣⵉⵗⵝ  ⵜⴰⵎⴰⵣⵉⵗⵜ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "56",
        iso6392: "bho",
        iso6395: "bho",
        name: "Bhojpuri",
        native: "भोजपुरी",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "57",
        iso6392: "bih",
        iso6391: "bh",
        name: "Bihari languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "58",
        iso6392: "bik",
        iso6395: "bik",
        name: "Bikol",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "59",
        iso6392: "bin",
        iso6395: "bin",
        name: "Bini  Edo",
        native: "Ẹ̀dó",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "60",
        iso6392: "bis",
        iso6395: "bis",
        iso6391: "bi",
        name: "Bislama",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "61",
        iso6392: "byn",
        iso6395: "byn",
        name: "Blin  Bilin",
        native: "ብሊና  ብሊን",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "62",
        iso6392: "zbl",
        iso6395: "zbl",
        name: "Blissymbols  Blissymbolics  Bliss",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "63",
        iso6392: "nob",
        iso6395: "nob",
        iso6391: "nb",
        name: "Bokmål, Norwegian  Norwegian Bokmål",
        native: "bokmål",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "64",
        iso6392: "bos",
        iso6395: "bos",
        iso6391: "bs",
        name: "Bosnian",
        native: "bosanski",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "65",
        iso6392: "bra",
        iso6395: "bra",
        name: "Braj",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "66",
        iso6392: "bre",
        iso6395: "bre",
        iso6391: "br",
        name: "Breton",
        native: "Brezhoneg",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "67",
        iso6392: "bug",
        iso6395: "bug",
        name: "Buginese",
        native: "ᨅᨔ ᨕᨘᨁᨗ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "68",
        iso6392: "bul",
        iso6395: "bul",
        iso6391: "bg",
        name: "Bulgarian",
        native: "български език",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "69",
        iso6392: "bua",
        iso6395: "bua",
        name: "Buriat",
        native: "буряад хэлэн",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "70",
        iso6392: "bur* / mya",
        iso6395: "mya",
        iso6391: "my",
        name: "Burmese",
        native: "မြန်မာစာ  မြန်မာစကား",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "71",
        iso6392: "mya / bur*",
        iso6395: "mya",
        iso6391: "my",
        name: "Burmese",
        native: "မြန်မာစာ  မြန်မာစကား",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "72",
        iso6392: "cad",
        iso6395: "cad",
        name: "Caddo",
        native: "Hasí:nay",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "73",
        iso6392: "cat",
        iso6395: "cat",
        iso6391: "ca",
        name: "Catalan  Valencian",
        native: "català",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "74",
        iso6392: "cau",
        name: "Caucasian languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "75",
        iso6392: "ceb",
        iso6395: "ceb",
        name: "Cebuano",
        native: "Sinugbuanong Binisayâ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "76",
        iso6392: "cel",
        name: "Celtic languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "77",
        iso6392: "cai",
        name: "Central American Indian languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "78",
        iso6392: "khm",
        iso6395: "khm",
        iso6391: "km",
        name: "Central Khmer",
        native: "ភាសាខ្មែរ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "79",
        iso6392: "chg",
        iso6395: "chg",
        name: "Chagatai",
        native: "جغتای",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "80",
        iso6392: "cmc",
        name: "Chamic languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "81",
        iso6392: "cha",
        iso6395: "cha",
        iso6391: "ch",
        name: "Chamorro",
        native: "Finu' Chamoru",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "82",
        iso6392: "che",
        iso6395: "che",
        iso6391: "ce",
        name: "Chechen",
        native: "Нохчийн мотт  نَاخچیین موٓتت  ნახჩიე მუოთთ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "83",
        iso6392: "chr",
        iso6395: "chr",
        name: "Cherokee",
        native: "ᏣᎳᎩ ᎦᏬᏂᎯᏍᏗ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "84",
        iso6392: "chy",
        iso6395: "chy",
        name: "Cheyenne",
        native: "Tsėhésenėstsestȯtse",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "85",
        iso6392: "chb",
        iso6395: "chb",
        name: "Chibcha",
        native: "Muysccubun",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "86",
        iso6392: "nya",
        iso6395: "nya",
        iso6391: "ny",
        name: "Chichewa  Chewa  Nyanja",
        native: "Chichewa  Chinyanja",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "88",
        iso6392: "zho / chi*",
        iso6395: "zho",
        iso6391: "zh",
        name: "Chinese",
        native: "中文  汉语  漢語",
        f_active: "1",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "89",
        iso6392: "chn",
        iso6395: "chn",
        name: "Chinook jargon",
        native: "chinuk wawa  wawa  chinook lelang  lelang",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "90",
        iso6392: "chp",
        iso6395: "chp",
        name: "Chipewyan  Dene Suline",
        native: "ᑌᓀᓱᒼᕄᓀ (Dënesųłiné)",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "91",
        iso6392: "cho",
        iso6395: "cho",
        name: "Choctaw",
        native: "Chahta'",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "92",
        iso6392: "chu",
        iso6395: "chu",
        iso6391: "cu",
        name: "Church Slavic  Old Slavonic  Church Slavonic  Old Bulgarian  Old Church Slavonic",
        native: "Славе́нскїй ѧ҆зы́къ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "93",
        iso6392: "chk",
        iso6395: "chk",
        name: "Chuukese",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "94",
        iso6392: "chv",
        iso6395: "chv",
        iso6391: "cv",
        name: "Chuvash",
        native: "Чӑвашла",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "95",
        iso6392: "syc",
        iso6395: "syc",
        name: "Classical Syriac",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "96",
        iso6392: "nwc",
        iso6395: "nwc",
        name: "Classical Newari  Old Newari  Classical Nepal Bhasa",
        native: "पुलां भाय्  पुलाङु नेपाल भाय्",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "97",
        iso6392: "cop",
        iso6395: "cop",
        name: "Coptic",
        native: "ϯⲙⲉⲑⲣⲉⲙⲛ̀ⲭⲏⲙⲓ  ⲧⲙⲛ̄ⲧⲣⲙ̄ⲛ̄ⲕⲏⲙⲉ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "98",
        iso6392: "cor",
        iso6395: "cor",
        iso6391: "kw",
        name: "Cornish",
        native: "Kernowek",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "99",
        iso6392: "cos",
        iso6395: "cos",
        iso6391: "co",
        name: "Corsican",
        native: "Corsu  Lingua corsa",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "100",
        iso6392: "cre",
        iso6395: "cre",
        iso6391: "cr",
        name: "Cree",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "101",
        iso6392: "mus",
        iso6395: "mus",
        name: "Creek",
        native: "Mvskoke",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "102",
        iso6392: "cpf",
        name: "Creoles and pidgins, French-based",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "103",
        iso6392: "cpp",
        name: "Creoles and pidgins, Portuguese-based",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "104",
        iso6392: "crp",
        name: "Creoles and pidgins",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "105",
        iso6392: "cpe",
        name: "Creoles and pidgins, English based",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "106",
        iso6392: "crh",
        iso6395: "crh",
        name: "Crimean Tatar  Crimean Turkish",
        native: "Къырымтатарджа  Къырымтатар тили  Ҡырымтатарҗа  Ҡырымтатар тили",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "107",
        iso6392: "hrv",
        iso6395: "hrv",
        iso6391: "hr",
        name: "Croatian",
        native: "hrvatski",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "108",
        iso6392: "cus",
        name: "Cushitic languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "109",
        iso6392: "ces / cze*",
        iso6395: "ces",
        iso6391: "cs",
        name: "Czech",
        native: "čeština  český jazyk",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "110",
        iso6392: "cze* / ces",
        iso6395: "ces",
        iso6391: "cs",
        name: "Czech",
        native: "čeština  český jazyk",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "111",
        iso6392: "dak",
        iso6395: "dak",
        name: "Dakota",
        native: "Dakhótiyapi  Dakȟótiyapi",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "112",
        iso6392: "dan",
        iso6395: "dan",
        iso6391: "da",
        name: "Danish",
        native: "dansk",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "113",
        iso6392: "dar",
        iso6395: "dar",
        name: "Dargwa",
        native: "дарган мез",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "114",
        iso6392: "del",
        iso6395: "del",
        name: "Delaware",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "115",
        iso6392: "din",
        iso6395: "din",
        name: "Dinka",
        native: "Thuɔŋjäŋ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "116",
        iso6392: "div",
        iso6395: "div",
        iso6391: "dv",
        name: "Divehi  Dhivehi  Maldivian",
        native: "ދިވެހި  ދިވެހިބަސް",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "117",
        iso6392: "doi",
        iso6395: "doi",
        name: "Dogri",
        native: "डोगरी  ڈوگرى",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "118",
        iso6392: "dgr",
        iso6395: "dgr",
        name: "Dogrib",
        native: "डोगरी  ڈوگرى",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "119",
        iso6392: "dra",
        name: "Dravidian languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "120",
        iso6392: "dua",
        iso6395: "dua",
        name: "Duala",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "121",
        iso6392: "dum",
        iso6395: "dum",
        name: "Dutch, Middle (ca. 1050–1350)",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "122",
        iso6392: "dut* / nld",
        iso6395: "nld",
        iso6391: "nl",
        name: "Dutch  Flemish",
        native: "Nederlands  Vlaams",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "123",
        iso6392: "nld / dut*",
        iso6395: "nld",
        iso6391: "nl",
        name: "Dutch  Flemish",
        native: "Nederlands  Vlaams",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "124",
        iso6392: "dyu",
        iso6395: "dyu",
        name: "Dyula",
        native: "Julakan",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "125",
        iso6392: "dzo",
        iso6395: "dzo",
        iso6391: "dz",
        name: "Dzongkha",
        native: "རྫོང་ཁ་",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "126",
        iso6392: "frs",
        iso6395: "frs",
        name: "Eastern Frisian",
        native: "Seeltersk",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "127",
        iso6392: "efi",
        iso6395: "efi",
        name: "Efik",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "128",
        iso6392: "egy",
        iso6395: "egy",
        name: "Egyptian (Ancient)",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "129",
        iso6392: "eka",
        iso6395: "eka",
        name: "Ekajuk",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "130",
        iso6392: "elx",
        iso6395: "elx",
        name: "Elamite",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "131",
        iso6392: "eng",
        iso6395: "eng",
        iso6391: "en",
        name: "English",
        native: "English",
        f_active: "1",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "132",
        iso6392: "enm",
        iso6395: "enm",
        name: "English, Middle (1100–1500)",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "133",
        iso6392: "ang",
        iso6395: "ang",
        name: "English, Old (ca.450–1100)",
        native: "Ænglisc  Anglisc  Englisc",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "134",
        iso6392: "myv",
        iso6395: "myv",
        name: "Erzya",
        native: "эрзянь кель",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "135",
        iso6392: "epo",
        iso6395: "epo",
        iso6391: "eo",
        name: "Esperanto",
        native: "Esperanto",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "136",
        iso6392: "est",
        iso6395: "est",
        iso6391: "et",
        name: "Estonian",
        native: "eesti keel",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "137",
        iso6392: "ewe",
        iso6395: "ewe",
        iso6391: "ee",
        name: "Ewe",
        native: "Èʋegbe",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "138",
        iso6392: "ewo",
        iso6395: "ewo",
        name: "Ewondo",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "139",
        iso6392: "fan",
        iso6395: "fan",
        name: "Fang",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "140",
        iso6392: "fat",
        iso6395: "fat",
        name: "Fanti",
        native: "Mfantse  Fante  Fanti",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "141",
        iso6392: "fao",
        iso6395: "fao",
        iso6391: "fo",
        name: "Faroese",
        native: "føroyskt",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "142",
        iso6392: "fij",
        iso6395: "fij",
        iso6391: "fj",
        name: "Fijian",
        native: "Na Vosa Vakaviti",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "143",
        iso6392: "fil",
        iso6395: "fil",
        name: "Filipino  Pilipino",
        native: "Wikang Filipino",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "144",
        iso6392: "fin",
        iso6395: "fin",
        iso6391: "fi",
        name: "Finnish",
        native: "suomen kieli",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "145",
        iso6392: "fiu",
        name: "Finno-Ugrian languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "146",
        iso6392: "fon",
        iso6395: "fon",
        name: "Fon",
        native: "Fon gbè",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "148",
        iso6392: "fre* / fra",
        iso6395: "fra",
        iso6391: "fr",
        name: "French",
        native: "français",
        f_active: "1",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "149",
        iso6392: "frm",
        iso6395: "frm",
        name: "French, Middle (ca. 1400–1600)",
        native: "françois  franceis",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "150",
        iso6392: "fro",
        iso6395: "fro",
        name: "French, Old (842–ca. 1400)",
        native: "Franceis  François  Romanz",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "151",
        iso6392: "fur",
        iso6395: "fur",
        name: "Friulian",
        native: "Furlan",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "152",
        iso6392: "ful",
        iso6395: "ful",
        iso6391: "ff",
        name: "Fulah",
        native: "Fulfulde  Pulaar  Pular",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "153",
        iso6392: "gaa",
        iso6395: "gaa",
        name: "Ga",
        native: "Gã",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "154",
        iso6392: "gla",
        iso6395: "gla",
        iso6391: "gd",
        name: "Gaelic  Scottish Gaelic",
        native: "Gàidhlig",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "155",
        iso6392: "car",
        iso6395: "car",
        name: "Galibi Carib",
        native: "Kari'nja",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "156",
        iso6392: "glg",
        iso6395: "glg",
        iso6391: "gl",
        name: "Galician",
        native: "galego",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "157",
        iso6392: "lug",
        iso6395: "lug",
        iso6391: "lg",
        name: "Ganda",
        native: "Luganda",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "158",
        iso6392: "gay",
        iso6395: "gay",
        name: "Gayo",
        native: "Basa Gayo",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "159",
        iso6392: "gba",
        iso6395: "gba",
        name: "Gbaya",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "160",
        iso6392: "gez",
        iso6395: "gez",
        name: "Geez",
        native: "ግዕዝ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "161",
        iso6392: "geo* / kat",
        iso6395: "kat",
        iso6391: "ka",
        name: "Georgian",
        native: "ქართული",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "162",
        iso6392: "kat / geo*",
        iso6395: "kat",
        iso6391: "ka",
        name: "Georgian",
        native: "ქართული",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "163",
        iso6392: "deu / ger*",
        iso6395: "deu",
        iso6391: "de",
        name: "German",
        native: "Deutsch",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "164",
        iso6392: "ger* / deu",
        iso6395: "deu",
        iso6391: "de",
        name: "German",
        native: "Deutsch",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "165",
        iso6392: "gmh",
        iso6395: "gmh",
        name: "German, Middle High (ca. 1050–1500)",
        native: "Diutsch",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "166",
        iso6392: "goh",
        iso6395: "goh",
        name: "German, Old High (ca. 750–1050)",
        native: "Diutisk",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "167",
        iso6392: "gem",
        name: "Germanic languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "168",
        iso6392: "gil",
        iso6395: "gil",
        name: "Gilbertese",
        native: "Taetae ni Kiribati",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "169",
        iso6392: "gon",
        iso6395: "gon",
        name: "Gondi",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "170",
        iso6392: "gor",
        iso6395: "gor",
        name: "Gorontalo",
        native: "Bahasa Hulontalo",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "171",
        iso6392: "got",
        iso6395: "got",
        name: "Gothic",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "172",
        iso6392: "grb",
        iso6395: "grb",
        name: "Grebo",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "173",
        iso6392: "grc",
        iso6395: "grc",
        name: "Greek, Ancient (to 1453)",
        native: "Ἑλληνική",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "174",
        iso6392: "ell / gre*",
        iso6395: "ell",
        iso6391: "el",
        name: "Greek, Modern (1453–)",
        native: "Νέα Ελληνικά",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "175",
        iso6392: "gre* / ell",
        iso6395: "ell",
        iso6391: "el",
        name: "Greek, Modern (1453–)",
        native: "Νέα Ελληνικά",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "176",
        iso6392: "grn",
        iso6395: "grn",
        iso6391: "gn",
        name: "Guarani",
        native: "Avañe'ẽ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "177",
        iso6392: "guj",
        iso6395: "guj",
        iso6391: "gu",
        name: "Gujarati",
        native: "ગુજરાતી",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "178",
        iso6392: "gwi",
        iso6395: "gwi",
        name: "Gwich'in",
        native: "Dinjii Zhu’ Ginjik",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "179",
        iso6392: "hai",
        iso6395: "hai",
        name: "Haida",
        native: "X̱aat Kíl  X̱aadas Kíl  X̱aayda Kil  Xaad kil",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "180",
        iso6392: "hat",
        iso6395: "hat",
        iso6391: "ht",
        name: "Haitian  Haitian Creole",
        native: "kreyòl ayisyen",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "181",
        iso6392: "hau",
        iso6395: "hau",
        iso6391: "ha",
        name: "Hausa",
        native: "Harshen Hausa  هَرْشَن",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "182",
        iso6392: "haw",
        iso6395: "haw",
        name: "Hawaiian",
        native: "ʻŌlelo Hawaiʻi",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "183",
        iso6392: "heb",
        iso6395: "heb",
        iso6391: "he",
        name: "Hebrew",
        native: "עברית",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "184",
        iso6392: "her",
        iso6395: "her",
        iso6391: "hz",
        name: "Herero",
        native: "Otjiherero",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "185",
        iso6392: "hil",
        iso6395: "hil",
        name: "Hiligaynon",
        native: "Ilonggo",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "186",
        iso6392: "him",
        name: "Himachali languages  Pahari languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "187",
        iso6392: "hin",
        iso6395: "hin",
        iso6391: "hi",
        name: "Hindi",
        native: "हिन्दी",
        f_active: "1",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "188",
        iso6392: "hmo",
        iso6395: "hmo",
        iso6391: "ho",
        name: "Hiri Motu",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "189",
        iso6392: "hit",
        iso6395: "hit",
        name: "Hittite",
        native: "𒉈𒅆𒇷",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "190",
        iso6392: "hmn",
        iso6395: "hmn",
        name: "Hmong  Mong",
        native: "lus Hmoob  lug Moob  lol Hmongb",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "191",
        iso6392: "hun",
        iso6395: "hun",
        iso6391: "hu",
        name: "Hungarian",
        native: "magyar nyelv",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "192",
        iso6392: "hup",
        iso6395: "hup",
        name: "Hupa",
        native: "Na:tinixwe Mixine:whe'",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "193",
        iso6392: "iba",
        iso6395: "iba",
        name: "Iban",
        native: "Jaku Iban",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "194",
        iso6392: "ice* / isl",
        iso6395: "isl",
        iso6391: "is",
        name: "Icelandic",
        native: "íslenska",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "195",
        iso6392: "isl / ice*",
        iso6395: "isl",
        iso6391: "is",
        name: "Icelandic",
        native: "íslenska",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "196",
        iso6392: "ido",
        iso6395: "ido",
        iso6391: "io",
        name: "Ido",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "197",
        iso6392: "ibo",
        iso6395: "ibo",
        iso6391: "ig",
        name: "Igbo",
        native: "Asụsụ Igbo",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "198",
        iso6392: "ijo",
        name: "Ijo languages",
        native: "Ịjọ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "199",
        iso6392: "ilo",
        iso6395: "ilo",
        name: "Iloko",
        native: "Pagsasao nga Ilokano  Ilokano",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "200",
        iso6392: "smn",
        iso6395: "smn",
        name: "Inari Sami",
        native: "anarâškielâ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "201",
        iso6392: "inc",
        name: "Indic languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "202",
        iso6392: "ine",
        name: "Indo-European languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "203",
        iso6392: "ind",
        iso6395: "ind",
        iso6391: "id",
        name: "Indonesian",
        native: "bahasa Indonesia",
        f_active: "1",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "204",
        iso6392: "inh",
        iso6395: "inh",
        name: "Ingush",
        native: "ГӀалгӀай мотт",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "205",
        iso6392: "ina",
        iso6395: "ina",
        iso6391: "ia",
        name: "Interlingua (International Auxiliary Language Association)",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "206",
        iso6392: "ile",
        iso6395: "ile",
        iso6391: "ie",
        name: "Interlingue  Occidental",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "207",
        iso6392: "iku",
        iso6395: "iku",
        iso6391: "iu",
        name: "Inuktitut",
        native: "ᐃᓄᒃᑎᑐᑦ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "208",
        iso6392: "ipk",
        iso6395: "ipk",
        iso6391: "ik",
        name: "Inupiaq",
        native: "Iñupiaq",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "209",
        iso6392: "ira",
        name: "Iranian languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "210",
        iso6392: "gle",
        iso6395: "gle",
        iso6391: "ga",
        name: "Irish",
        native: "Gaeilge",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "211",
        iso6392: "mga",
        iso6395: "mga",
        name: "Irish, Middle (900–1200)",
        native: "Gaoidhealg",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "212",
        iso6392: "sga",
        iso6395: "sga",
        name: "Irish, Old (to 900)",
        native: "Goídelc",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "213",
        iso6392: "iro",
        name: "Iroquoian languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "214",
        iso6392: "ita",
        iso6395: "ita",
        iso6391: "it",
        name: "Italian",
        native: "italiano  lingua italiana",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "215",
        iso6392: "jpn",
        iso6395: "jpn",
        iso6391: "ja",
        name: "Japanese",
        native: "日本語",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "216",
        iso6392: "jav",
        iso6395: "jav",
        iso6391: "jv",
        name: "Javanese",
        native: "ꦧꦱꦗꦮ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "217",
        iso6392: "jrb",
        iso6395: "jrb",
        name: "Judeo-Arabic",
        native: "عربية يهودية / ערבית יהודית",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "218",
        iso6392: "jpr",
        iso6395: "jpr",
        name: "Judeo-Persian",
        native: "Dzhidi",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "219",
        iso6392: "kbd",
        iso6395: "kbd",
        name: "Kabardian",
        native: "Адыгэбзэ (Къэбэрдейбзэ)",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "220",
        iso6392: "kab",
        iso6395: "kab",
        name: "Kabyle",
        native: "Tamaziɣt Taqbaylit  Tazwawt",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "221",
        iso6392: "kac",
        iso6395: "kac",
        name: "Kachin  Jingpho",
        native: "Jingpho",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "222",
        iso6392: "kal",
        iso6395: "kal",
        iso6391: "kl",
        name: "Kalaallisut  Greenlandic",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "223",
        iso6392: "xal",
        iso6395: "xal",
        name: "Kalmyk  Oirat",
        native: "Хальмг келн / Xaľmg keln",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "224",
        iso6392: "kam",
        iso6395: "kam",
        name: "Kamba",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "225",
        iso6392: "kan",
        iso6395: "kan",
        iso6391: "kn",
        name: "Kannada",
        native: "ಕನ್ನಡ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "226",
        iso6392: "kau",
        iso6395: "kau",
        iso6391: "kr",
        name: "Kanuri",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "227",
        iso6392: "krc",
        iso6395: "krc",
        name: "Karachay-Balkar",
        native: "Къарачай-Малкъар тил  Таулу тил",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "228",
        iso6392: "kaa",
        iso6395: "kaa",
        name: "Kara-Kalpak",
        native: "Qaraqalpaq tili  Қарақалпақ тили",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "229",
        iso6392: "krl",
        iso6395: "krl",
        name: "Karelian",
        native: "karjal  kariela  karjala",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "230",
        iso6392: "kar",
        name: "Karen languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "231",
        iso6392: "kas",
        iso6395: "kas",
        iso6391: "ks",
        name: "Kashmiri",
        native: "कॉशुर / كأشُر",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "232",
        iso6392: "csb",
        iso6395: "csb",
        name: "Kashubian",
        native: "Kaszëbsczi jãzëk",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "233",
        iso6392: "kaw",
        iso6395: "kaw",
        name: "Kawi",
        native: "ꦧꦱꦗꦮ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "234",
        iso6392: "kaz",
        iso6395: "kaz",
        iso6391: "kk",
        name: "Kazakh",
        native: "қазақ тілі / qazaq tili",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "235",
        iso6392: "kha",
        iso6395: "kha",
        name: "Khasi",
        native: "কা কতিয়েন খাশি",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "236",
        iso6392: "khi",
        name: "Khoisan languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "237",
        iso6392: "kho",
        iso6395: "kho",
        name: "Khotanese  Sakan",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "238",
        iso6392: "kik",
        iso6395: "kik",
        iso6391: "ki",
        name: "Kikuyu  Gikuyu",
        native: "Gĩkũyũ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "239",
        iso6392: "kmb",
        iso6395: "kmb",
        name: "Kimbundu",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "240",
        iso6392: "kin",
        iso6395: "kin",
        iso6391: "rw",
        name: "Kinyarwanda",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "241",
        iso6392: "kir",
        iso6395: "kir",
        iso6391: "ky",
        name: "Kirghiz  Kyrgyz",
        native: "кыргызча  кыргыз тили",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "242",
        iso6392: "tlh",
        iso6395: "tlh",
        name: "Klingon  tlhIngan-Hol",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "243",
        iso6392: "kom",
        iso6395: "kom",
        iso6391: "kv",
        name: "Komi",
        native: "Коми кыв",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "244",
        iso6392: "kon",
        iso6395: "kon",
        iso6391: "kg",
        name: "Kongo",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "245",
        iso6392: "kok",
        iso6395: "kok",
        name: "Konkani",
        native: "कोंकणी",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "246",
        iso6392: "kor",
        iso6395: "kor",
        iso6391: "ko",
        name: "Korean",
        native: "한국어",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "247",
        iso6392: "kos",
        iso6395: "kos",
        name: "Kosraean",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "248",
        iso6392: "kpe",
        iso6395: "kpe",
        name: "Kpelle",
        native: "Kpɛlɛwoo",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "249",
        iso6392: "kro",
        name: "Kru languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "250",
        iso6392: "kua",
        iso6395: "kua",
        iso6391: "kj",
        name: "Kuanyama  Kwanyama",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "251",
        iso6392: "kum",
        iso6395: "kum",
        name: "Kumyk",
        native: "къумукъ тил/qumuq til",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "252",
        iso6392: "kur",
        iso6395: "kur",
        iso6391: "ku",
        name: "Kurdish",
        native: "Kurdî / کوردی",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "253",
        iso6392: "kru",
        iso6395: "kru",
        name: "Kurukh",
        native: "कुड़ुख़",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "254",
        iso6392: "kut",
        iso6395: "kut",
        name: "Kutenai",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "255",
        iso6392: "lad",
        iso6395: "lad",
        name: "Ladino",
        native: "Judeo-español",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "256",
        iso6392: "lah",
        iso6395: "lah",
        name: "Lahnda",
        native: "بھارت کا",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "257",
        iso6392: "lam",
        iso6395: "lam",
        name: "Lamba",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "258",
        iso6392: "day",
        name: "Land Dayak languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "259",
        iso6392: "lao",
        iso6395: "lao",
        iso6391: "lo",
        name: "Lao",
        native: "ພາສາລາວ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "260",
        iso6392: "lat",
        iso6395: "lat",
        iso6391: "la",
        name: "Latin",
        native: "Lingua latīna",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "261",
        iso6392: "lav",
        iso6395: "lav",
        iso6391: "lv",
        name: "Latvian",
        native: "Latviešu valoda",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "262",
        iso6392: "lez",
        iso6395: "lez",
        name: "Lezghian",
        native: "Лезги чӏал",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "263",
        iso6392: "lim",
        iso6395: "lim",
        iso6391: "li",
        name: "Limburgan  Limburger  Limburgish",
        native: "Lèmburgs",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "264",
        iso6392: "lin",
        iso6395: "lin",
        iso6391: "ln",
        name: "Lingala",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "265",
        iso6392: "lit",
        iso6395: "lit",
        iso6391: "lt",
        name: "Lithuanian",
        native: "lietuvių kalba",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "266",
        iso6392: "jbo",
        iso6395: "jbo",
        name: "Lojban",
        native: "la .lojban.",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "267",
        iso6392: "nds",
        iso6395: "nds",
        name: "Low German  Low Saxon  German, Low  Saxon, Low",
        native: "Plattdütsch  Plattdüütsch",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "268",
        iso6392: "dsb",
        iso6395: "dsb",
        name: "Lower Sorbian",
        native: "Dolnoserbski  Dolnoserbšćina",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "269",
        iso6392: "loz",
        iso6395: "loz",
        name: "Lozi",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "270",
        iso6392: "lub",
        iso6395: "lub",
        iso6391: "lu",
        name: "Luba-Katanga",
        native: "Kiluba",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "271",
        iso6392: "lua",
        iso6395: "lua",
        name: "Luba-Lulua",
        native: "Tshiluba",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "272",
        iso6392: "lui",
        iso6395: "lui",
        name: "Luiseno",
        native: "Cham'teela",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "273",
        iso6392: "smj",
        iso6395: "smj",
        name: "Lule Sami",
        native: "julevsámegiella",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "274",
        iso6392: "lun",
        iso6395: "lun",
        name: "Lunda",
        native: "Chilunda",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "275",
        iso6392: "luo",
        iso6395: "luo",
        name: "Luo (Kenya and Tanzania)",
        native: "Dholuo",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "276",
        iso6392: "lus",
        iso6395: "lus",
        name: "Lushai",
        native: "Mizo ṭawng",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "277",
        iso6392: "ltz",
        iso6395: "ltz",
        iso6391: "lb",
        name: "Luxembourgish  Letzeburgesch",
        native: "Lëtzebuergesch",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "278",
        iso6392: "mac* / mkd",
        iso6395: "mkd",
        iso6391: "mk",
        name: "Macedonian",
        native: "македонски јазик",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "279",
        iso6392: "mkd / mac*",
        iso6395: "mkd",
        iso6391: "mk",
        name: "Macedonian",
        native: "македонски јазик",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "280",
        iso6392: "mad",
        iso6395: "mad",
        name: "Madurese",
        native: "Madhura",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "281",
        iso6392: "mag",
        iso6395: "mag",
        name: "Magahi",
        native: "मगही",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "282",
        iso6392: "mai",
        iso6395: "mai",
        name: "Maithili",
        native: "मैथिली  মৈথিলী",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "283",
        iso6392: "mak",
        iso6395: "mak",
        name: "Makasar",
        native: "Basa Mangkasara' / ᨅᨔ ᨆᨀᨔᨑ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "284",
        iso6392: "mlg",
        iso6395: "mlg",
        iso6391: "mg",
        name: "Malagasy",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "285",
        iso6392: "may* / msa",
        iso6395: "msa",
        iso6391: "ms",
        name: "Malay",
        native: "Bahasa Melayu",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "286",
        iso6392: "msa / may*",
        iso6395: "msa",
        iso6391: "ms",
        name: "Malay",
        native: "Bahasa Melayu",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "287",
        iso6392: "mal",
        iso6395: "mal",
        iso6391: "ml",
        name: "Malayalam",
        native: "മലയാളം",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "288",
        iso6392: "mlt",
        iso6395: "mlt",
        iso6391: "mt",
        name: "Maltese",
        native: "Malti",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "289",
        iso6392: "mnc",
        iso6395: "mnc",
        name: "Manchu",
        native: "ᠮᠠᠨᠵᡠ ᡤᡳᠰᡠᠨ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "290",
        iso6392: "mdr",
        iso6395: "mdr",
        name: "Mandar",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "291",
        iso6392: "man",
        iso6395: "man",
        name: "Mandingo",
        native: "Mandi'nka kango",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "292",
        iso6392: "mni",
        iso6395: "mni",
        name: "Manipuri",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "293",
        iso6392: "mno",
        name: "Manobo languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "294",
        iso6392: "glv",
        iso6395: "glv",
        iso6391: "gv",
        name: "Manx",
        native: "Gaelg  Gailck",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "295",
        iso6392: "mao* / mri",
        iso6395: "mri",
        iso6391: "mi",
        name: "Maori",
        native: "Te Reo Māori",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "296",
        iso6392: "mri / mao*",
        iso6395: "mri",
        iso6391: "mi",
        name: "Maori",
        native: "Te Reo Māori",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "297",
        iso6392: "arn",
        iso6395: "arn",
        name: "Mapudungun  Mapuche",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "298",
        iso6392: "mar",
        iso6395: "mar",
        iso6391: "mr",
        name: "Marathi",
        native: "मराठी",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "299",
        iso6392: "chm",
        iso6395: "chm",
        name: "Mari",
        native: "марий йылме",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "300",
        iso6392: "mah",
        iso6395: "mah",
        iso6391: "mh",
        name: "Marshallese",
        native: "Kajin M̧ajeļ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "301",
        iso6392: "mwr",
        iso6395: "mwr",
        name: "Marwari",
        native: "मारवाड़ी",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "302",
        iso6392: "mas",
        iso6395: "mas",
        name: "Masai",
        native: "ɔl",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "303",
        iso6392: "myn",
        name: "Mayan languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "304",
        iso6392: "men",
        iso6395: "men",
        name: "Mende",
        native: "Mɛnde yia",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "305",
        iso6392: "mic",
        iso6395: "mic",
        name: "Mi'kmaq  Micmac",
        native: "Míkmawísimk",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "306",
        iso6392: "min",
        iso6395: "min",
        name: "Minangkabau",
        native: "Baso Minang",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "307",
        iso6392: "mwl",
        iso6395: "mwl",
        name: "Mirandese",
        native: "mirandés  lhéngua mirandesa",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "308",
        iso6392: "moh",
        iso6395: "moh",
        name: "Mohawk",
        native: "Kanien’kéha",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "309",
        iso6392: "mdf",
        iso6395: "mdf",
        name: "Moksha",
        native: "мокшень кяль",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "310",
        iso6392: "lol",
        iso6395: "lol",
        name: "Mongo",
        native: "Lomongo",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "311",
        iso6392: "mon",
        iso6395: "mon",
        iso6391: "mn",
        name: "Mongolian",
        native: "монгол хэл  ᠮᠣᠩᠭᠣᠯ ᠬᠡᠯᠡ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "312",
        iso6392: "mkh",
        name: "Mon-Khmer languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "313",
        iso6392: "cnr",
        iso6395: "cnr",
        name: "Montenegrin",
        native: "crnogorski / црногорски",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "314",
        iso6392: "mos",
        iso6395: "mos",
        name: "Mossi",
        native: "Mooré",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "315",
        iso6392: "mul",
        iso6395: "mul",
        name: "Multiple languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "316",
        iso6392: "mun",
        name: "Munda languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "317",
        iso6392: "nah",
        name: "Nahuatl languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "318",
        iso6392: "nau",
        iso6395: "nau",
        iso6391: "na",
        name: "Nauru",
        native: "dorerin Naoero",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "319",
        iso6392: "nav",
        iso6395: "nav",
        iso6391: "nv",
        name: "Navajo  Navaho",
        native: "Diné bizaad  Naabeehó bizaad",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "320",
        iso6392: "nde",
        iso6395: "nde",
        iso6391: "nd",
        name: "Ndebele, North  North Ndebele",
        native: "siNdebele saseNyakatho",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "321",
        iso6392: "nbl",
        iso6395: "nbl",
        iso6391: "nr",
        name: "Ndebele, South  South Ndebele",
        native: "isiNdebele seSewula",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "322",
        iso6392: "ndo",
        iso6395: "ndo",
        iso6391: "ng",
        name: "Ndonga",
        native: "ndonga",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "323",
        iso6392: "nap",
        iso6395: "nap",
        name: "Neapolitan",
        native: "napulitano",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "324",
        iso6392: "new",
        iso6395: "new",
        name: "Nepal Bhasa  Newari",
        native: "नेपाल भाषा  नेवाः भाय्",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "325",
        iso6392: "nep",
        iso6395: "nep",
        iso6391: "ne",
        name: "Nepali",
        native: "नेपाली भाषा",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "326",
        iso6392: "nia",
        iso6395: "nia",
        name: "Nias",
        native: "Li Niha",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "327",
        iso6392: "nic",
        name: "Niger-Kordofanian languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "328",
        iso6392: "ssa",
        name: "Nilo-Saharan languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "329",
        iso6392: "niu",
        iso6395: "niu",
        name: "Niuean",
        native: "ko e vagahau Niuē",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "330",
        iso6392: "nqo",
        iso6395: "nqo",
        name: "N'Ko",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "331",
        iso6392: "zxx",
        iso6395: "zxx",
        name: "No linguistic content  Not applicable",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "332",
        iso6392: "nog",
        iso6395: "nog",
        name: "Nogai",
        native: "Ногай тили",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "333",
        iso6392: "non",
        iso6395: "non",
        name: "Norse, Old",
        native: "Dǫnsk tunga  Norrœnt mál",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "334",
        iso6392: "nai",
        name: "North American Indian languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "335",
        iso6392: "frr",
        iso6395: "frr",
        name: "Northern Frisian",
        native: "Frasch  Fresk  Freesk  Friisk",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "336",
        iso6392: "sme",
        iso6395: "sme",
        iso6391: "se",
        name: "Northern Sami",
        native: "davvisámegiella",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "337",
        iso6392: "nor",
        iso6395: "nor",
        iso6391: "no",
        name: "Norwegian",
        native: "norsk",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "338",
        iso6392: "nno",
        iso6395: "nno",
        iso6391: "nn",
        name: "Norwegian Nynorsk  Nynorsk, Norwegian",
        native: "nynorsk",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "339",
        iso6392: "nub",
        name: "Nubian languages",
        native: "لغات نوبية",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "340",
        iso6392: "nym",
        iso6395: "nym",
        name: "Nyamwezi",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "341",
        iso6392: "nyn",
        iso6395: "nyn",
        name: "Nyankole",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "342",
        iso6392: "nyo",
        iso6395: "nyo",
        name: "Nyoro",
        native: "Runyoro",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "343",
        iso6392: "nzi",
        iso6395: "nzi",
        name: "Nzima",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "344",
        iso6392: "oci",
        iso6395: "oci",
        iso6391: "oc",
        name: "Occitan (post 1500)  Provençal",
        native: "occitan  lenga d'òc  provençal",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "345",
        iso6392: "arc",
        iso6395: "arc",
        name: "Official Aramaic (700–300 BCE)  Imperial Aramaic (700–300 BCE)",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "346",
        iso6392: "oji",
        iso6395: "oji",
        iso6391: "oj",
        name: "Ojibwa",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "347",
        iso6392: "ori",
        iso6395: "ori",
        iso6391: "or",
        name: "Oriya",
        native: "ଓଡ଼ିଆ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "348",
        iso6392: "orm",
        iso6395: "orm",
        iso6391: "om",
        name: "Oromo",
        native: "Afaan Oromoo",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "349",
        iso6392: "osa",
        iso6395: "osa",
        name: "Osage",
        native: "Wazhazhe ie / 𐓏𐓘𐓻𐓘𐓻𐓟 𐒻𐓟",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "350",
        iso6392: "oss",
        iso6395: "oss",
        iso6391: "os",
        name: "Ossetian  Ossetic",
        native: "Ирон æвзаг",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "351",
        iso6392: "oto",
        name: "Otomian languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "352",
        iso6392: "pal",
        iso6395: "pal",
        name: "Pahlavi",
        native: "Pārsīk  Pārsīg",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "353",
        iso6392: "pau",
        iso6395: "pau",
        name: "Palauan",
        native: "a tekoi er a Belau",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "354",
        iso6392: "pli",
        iso6395: "pli",
        iso6391: "pi",
        name: "Pali",
        native: "Pāli",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "355",
        iso6392: "pam",
        iso6395: "pam",
        name: "Pampanga  Kapampangan",
        native: "Amánung Kapampangan  Amánung Sísuan",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "356",
        iso6392: "pag",
        iso6395: "pag",
        name: "Pangasinan",
        native: "Salitan Pangasinan",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "357",
        iso6392: "pan",
        iso6395: "pan",
        iso6391: "pa",
        name: "Panjabi  Punjabi",
        native: "ਪੰਜਾਬੀ / پنجابی",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "358",
        iso6392: "pap",
        iso6395: "pap",
        name: "Papiamento",
        native: "Papiamentu",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "359",
        iso6392: "paa",
        name: "Papuan languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "360",
        iso6392: "nso",
        iso6395: "nso",
        name: "Pedi  Sepedi  Northern Sotho",
        native: "Sesotho sa Leboa",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "361",
        iso6392: "fas / per*",
        iso6395: "fas",
        iso6391: "fa",
        name: "Persian",
        native: "فارسی",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "362",
        iso6392: "per* / fas",
        iso6395: "fas",
        iso6391: "fa",
        name: "Persian",
        native: "فارسی",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "363",
        iso6392: "peo",
        iso6395: "peo",
        name: "Persian, Old (ca. 600–400 B.C.)",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "364",
        iso6392: "phi",
        name: "Philippine languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "365",
        iso6392: "phn",
        iso6395: "phn",
        name: "Phoenician",
        native: "𐤃𐤁𐤓𐤉𐤌 𐤊𐤍𐤏𐤍𐤉𐤌",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "366",
        iso6392: "pon",
        iso6395: "pon",
        name: "Pohnpeian",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "367",
        iso6392: "pol",
        iso6395: "pol",
        iso6391: "pl",
        name: "Polish",
        native: "Język polski",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "368",
        iso6392: "por",
        iso6395: "por",
        iso6391: "pt",
        name: "Portuguese",
        native: "português",
        f_active: "1",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "369",
        iso6392: "pra",
        name: "Prakrit languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "370",
        iso6392: "pro",
        iso6395: "pro",
        name: "Provençal, Old (to 1500)  Old Occitan (to 1500)",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "371",
        iso6392: "pus",
        iso6395: "pus",
        iso6391: "ps",
        name: "Pushto  Pashto",
        native: "پښتو",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "372",
        iso6392: "que",
        iso6395: "que",
        iso6391: "qu",
        name: "Quechua",
        native: "Runa simi  kichwa simi  Nuna shimi",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "373",
        iso6392: "raj",
        iso6395: "raj",
        name: "Rajasthani",
        native: "राजस्थानी",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "374",
        iso6392: "rap",
        iso6395: "rap",
        name: "Rapanui",
        native: "Vananga rapa nui",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "375",
        iso6392: "rar",
        iso6395: "rar",
        name: "Rarotongan  Cook Islands Maori",
        native: "Māori Kūki 'Āirani",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "376",
        iso6392: "qaa-qtz",
        iso6395: "qaa-qtz",
        name: "Reserved for local use",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "377",
        iso6392: "roa",
        name: "Romance languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "378",
        iso6392: "ron / rum*",
        iso6395: "ron",
        iso6391: "ro",
        name: "Romanian  Moldavian  Moldovan",
        native: "limba română",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "379",
        iso6392: "rum* / ron",
        iso6395: "ron",
        iso6391: "ro",
        name: "Romanian  Moldavian  Moldovan",
        native: "limba română",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "380",
        iso6392: "roh",
        iso6395: "roh",
        iso6391: "rm",
        name: "Romansh",
        native: "Rumantsch  Rumàntsch  Romauntsch  Romontsch",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "381",
        iso6392: "rom",
        iso6395: "rom",
        name: "Romany",
        native: "romani čhib",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "382",
        iso6392: "run",
        iso6395: "run",
        iso6391: "rn",
        name: "Rundi",
        native: "Ikirundi",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "383",
        iso6392: "rus",
        iso6395: "rus",
        iso6391: "ru",
        name: "Russian",
        native: "русский язык",
        f_active: "1",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "384",
        iso6392: "sal",
        name: "Salishan languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "385",
        iso6392: "sam",
        iso6395: "sam",
        name: "Samaritan Aramaic",
        native: "ארמית",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "386",
        iso6392: "smi",
        name: "Sami languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "387",
        iso6392: "smo",
        iso6395: "smo",
        iso6391: "sm",
        name: "Samoan",
        native: "Gagana faʻa Sāmoa",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "388",
        iso6392: "sad",
        iso6395: "sad",
        name: "Sandawe",
        native: "Sandaweeki",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "389",
        iso6392: "sag",
        iso6395: "sag",
        iso6391: "sg",
        name: "Sango",
        native: "yângâ tî sängö",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "390",
        iso6392: "san",
        iso6395: "san",
        iso6391: "sa",
        name: "Sanskrit",
        native: "संस्कृतम्  𑌸𑌂𑌸𑍍𑌕𑍃𑌤𑌮𑍍",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "391",
        iso6392: "sat",
        iso6395: "sat",
        name: "Santali",
        native: "ᱥᱟᱱᱛᱟᱲᱤ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "392",
        iso6392: "srd",
        iso6395: "srd",
        iso6391: "sc",
        name: "Sardinian",
        native: "sardu  limba sarda  lingua sarda",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "393",
        iso6392: "sas",
        iso6395: "sas",
        name: "Sasak",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "394",
        iso6392: "sco",
        iso6395: "sco",
        name: "Scots",
        native: "Braid Scots  Lallans",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "395",
        iso6392: "sel",
        iso6395: "sel",
        name: "Selkup",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "396",
        iso6392: "sem",
        name: "Semitic languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "397",
        iso6392: "srp",
        iso6395: "srp",
        iso6391: "sr",
        name: "Serbian",
        native: "српски / srpski",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "398",
        iso6392: "srr",
        iso6395: "srr",
        name: "Serer",
        native: "Seereer",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "399",
        iso6392: "shn",
        iso6395: "shn",
        name: "Shan",
        native: "ၵႂၢမ်းတႆးယႂ်",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "400",
        iso6392: "sna",
        iso6395: "sna",
        iso6391: "sn",
        name: "Shona",
        native: "chiShona",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "401",
        iso6392: "iii",
        iso6395: "iii",
        iso6391: "ii",
        name: "Sichuan Yi  Nuosu",
        native: "ꆈꌠꉙ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "402",
        iso6392: "scn",
        iso6395: "scn",
        name: "Sicilian",
        native: "Sicilianu",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "403",
        iso6392: "sid",
        iso6395: "sid",
        name: "Sidamo",
        native: "Sidaamu Afoo",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "404",
        iso6392: "sgn",
        name: "Sign Languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "405",
        iso6392: "bla",
        iso6395: "bla",
        name: "Siksika",
        native: "ᓱᖽᐧᖿ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "406",
        iso6392: "snd",
        iso6395: "snd",
        iso6391: "sd",
        name: "Sindhi",
        native: "سنڌي / सिन्धी / ਸਿੰਧੀ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "407",
        iso6392: "sin",
        iso6395: "sin",
        iso6391: "si",
        name: "Sinhala  Sinhalese",
        native: "සිංහල",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "408",
        iso6392: "sit",
        name: "Sino-Tibetan languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "409",
        iso6392: "sio",
        name: "Siouan languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "410",
        iso6392: "sms",
        iso6395: "sms",
        name: "Skolt Sami",
        native: "sääʹmǩiõll",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "411",
        iso6392: "den",
        iso6395: "den",
        name: "Slave (Athapascan)",
        native: "Dene K'e",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "412",
        iso6392: "sla",
        name: "Slavic languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "413",
        iso6392: "slo* / slk",
        iso6395: "slk",
        iso6391: "sk",
        name: "Slovak",
        native: "slovenčina  slovenský jazyk",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "414",
        iso6392: "slk / slo*",
        iso6395: "slk",
        iso6391: "sk",
        name: "Slovak",
        native: "slovenčina  slovenský jazyk",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "415",
        iso6392: "slv",
        iso6395: "slv",
        iso6391: "sl",
        name: "Slovenian",
        native: "slovenski jezik  slovenščina",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "416",
        iso6392: "sog",
        iso6395: "sog",
        name: "Sogdian",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "417",
        iso6392: "som",
        iso6395: "som",
        iso6391: "so",
        name: "Somali",
        native: "af Soomaali",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "418",
        iso6392: "son",
        name: "Songhai languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "419",
        iso6392: "snk",
        iso6395: "snk",
        name: "Soninke",
        native: "Sooninkanxanne",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "420",
        iso6392: "wen",
        name: "Sorbian languages",
        native: "Serbsce / Serbski",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "421",
        iso6392: "sot",
        iso6395: "sot",
        iso6391: "st",
        name: "Sotho, Southern",
        native: "Sesotho [southern]",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "422",
        iso6392: "sai",
        name: "South American Indian (Other)",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "423",
        iso6392: "alt",
        iso6395: "alt",
        name: "Southern Altai",
        native: "Алтай тили",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "424",
        iso6392: "sma",
        iso6395: "sma",
        name: "Southern Sami",
        native: "Åarjelsaemien gïele",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "425",
        iso6392: "spa",
        iso6395: "spa",
        iso6391: "es",
        name: "Spanish",
        native: "español",
        f_active: "1",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "426",
        iso6392: "srn",
        iso6395: "srn",
        name: "Sranan Tongo",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "427",
        iso6392: "zgh",
        iso6395: "zgh",
        name: "Standard Moroccan Tamazight",
        native: "ⵜⴰⵎⴰⵣⵉⵖⵜ ⵜⴰⵏⴰⵡⴰⵢⵜ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "428",
        iso6392: "suk",
        iso6395: "suk",
        name: "Sukuma",
        native: "Kɪsukuma",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "429",
        iso6392: "sux",
        iso6395: "sux",
        name: "Sumerian",
        native: "𒅴𒂠",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "430",
        iso6392: "sun",
        iso6395: "sun",
        iso6391: "su",
        name: "Sundanese",
        native: "ᮘᮞ ᮞᮥᮔ᮪ᮓ / Basa Sunda",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "431",
        iso6392: "sus",
        iso6395: "sus",
        name: "Susu",
        native: "Sosoxui",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "432",
        iso6392: "swa",
        iso6395: "swa",
        iso6391: "sw",
        name: "Swahili",
        native: "Kiswahili",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "433",
        iso6392: "ssw",
        iso6395: "ssw",
        iso6391: "ss",
        name: "Swati",
        native: "siSwati",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "434",
        iso6392: "swe",
        iso6395: "swe",
        iso6391: "sv",
        name: "Swedish",
        native: "svenska",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "435",
        iso6392: "gsw",
        iso6395: "gsw",
        name: "Swiss German  Alemannic  Alsatian",
        native: "Schwiizerdütsch",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "436",
        iso6392: "syr",
        iso6395: "syr",
        name: "Syriac",
        native: "ܠܫܢܐ ܣܘܪܝܝܐ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "437",
        iso6392: "tgl",
        iso6395: "tgl",
        iso6391: "tl",
        name: "Tagalog",
        native: "Wikang Tagalog",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "438",
        iso6392: "tah",
        iso6395: "tah",
        iso6391: "ty",
        name: "Tahitian",
        native: "Reo Tahiti  Reo Mā'ohi",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "439",
        iso6392: "tai",
        name: "Tai languages",
        native: "ภาษาไท  ภาษาไต",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "440",
        iso6392: "tgk",
        iso6395: "tgk",
        iso6391: "tg",
        name: "Tajik",
        native: "тоҷикӣ / tojikī",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "441",
        iso6392: "tmh",
        iso6395: "tmh",
        name: "Tamashek",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "442",
        iso6392: "tam",
        iso6395: "tam",
        iso6391: "ta",
        name: "Tamil",
        native: "தமிழ்",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "443",
        iso6392: "tat",
        iso6395: "tat",
        iso6391: "tt",
        name: "Tatar",
        native: "татар теле / tatar tele / تاتار",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "444",
        iso6392: "tel",
        iso6395: "tel",
        iso6391: "te",
        name: "Telugu",
        native: "తెలుగు",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "445",
        iso6392: "ter",
        iso6395: "ter",
        name: "Tereno",
        native: "Terêna",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "446",
        iso6392: "tet",
        iso6395: "tet",
        name: "Tetum",
        native: "Lia-Tetun",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "447",
        iso6392: "tha",
        iso6395: "tha",
        iso6391: "th",
        name: "Thai",
        native: "ภาษาไทย",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "448",
        iso6392: "bod / tib*",
        iso6395: "bod",
        iso6391: "bo",
        name: "Tibetan",
        native: "བོད་སྐད་  ལྷ་སའི་སྐད་",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "449",
        iso6392: "tib* / bod",
        iso6395: "bod",
        iso6391: "bo",
        name: "Tibetan",
        native: "བོད་སྐད་  ལྷ་སའི་སྐད་",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "450",
        iso6392: "tig",
        iso6395: "tig",
        name: "Tigre",
        native: "ትግረ  ትግሬ  ኻሳ  ትግራይት",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "451",
        iso6392: "tir",
        iso6395: "tir",
        iso6391: "ti",
        name: "Tigrinya",
        native: "ትግርኛ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "452",
        iso6392: "tem",
        iso6395: "tem",
        name: "Timne",
        native: "KʌThemnɛ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "453",
        iso6392: "tiv",
        iso6395: "tiv",
        name: "Tiv",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "454",
        iso6392: "tli",
        iso6395: "tli",
        name: "Tlingit",
        native: "Lingít",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "455",
        iso6392: "tpi",
        iso6395: "tpi",
        name: "Tok Pisin",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "456",
        iso6392: "tkl",
        iso6395: "tkl",
        name: "Tokelau",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "457",
        iso6392: "tog",
        iso6395: "tog",
        name: "Tonga (Nyasa)",
        native: "chiTonga",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "458",
        iso6392: "ton",
        iso6395: "ton",
        iso6391: "to",
        name: "Tonga (Tonga Islands)",
        native: "lea faka-Tonga",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "459",
        iso6392: "tsi",
        iso6395: "tsi",
        name: "Tsimshian",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "460",
        iso6392: "tso",
        iso6395: "tso",
        iso6391: "ts",
        name: "Tsonga",
        native: "Xitsonga",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "461",
        iso6392: "tsn",
        iso6395: "tsn",
        iso6391: "tn",
        name: "Tswana",
        native: "Setswana",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "462",
        iso6392: "tum",
        iso6395: "tum",
        name: "Tumbuka",
        native: "chiTumbuka",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "463",
        iso6392: "tup",
        name: "Tupi languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "464",
        iso6392: "tur",
        iso6395: "tur",
        iso6391: "tr",
        name: "Turkish",
        native: "Türkçe",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "465",
        iso6392: "ota",
        iso6395: "ota",
        name: "Turkish, Ottoman (1500–1928)",
        native: "لسان عثمانى / lisân-ı Osmânî",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "466",
        iso6392: "tuk",
        iso6395: "tuk",
        iso6391: "tk",
        name: "Turkmen",
        native: "Türkmençe / Түркменче / تورکمن تیلی تورکمنچ  türkmen dili / түркмен дили",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "467",
        iso6392: "tvl",
        iso6395: "tvl",
        name: "Tuvalu",
        native: "Te Ggana Tuuvalu  Te Gagana Tuuvalu",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "468",
        iso6392: "tyv",
        iso6395: "tyv",
        name: "Tuvinian",
        native: "тыва дыл",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "469",
        iso6392: "twi",
        iso6395: "twi",
        iso6391: "tw",
        name: "Twi",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "470",
        iso6392: "udm",
        iso6395: "udm",
        name: "Udmurt",
        native: "удмурт кыл",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "471",
        iso6392: "uga",
        iso6395: "uga",
        name: "Ugaritic",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "472",
        iso6392: "uig",
        iso6395: "uig",
        iso6391: "ug",
        name: "Uighur  Uyghur",
        native: "ئۇيغۇرچە   ئۇيغۇر تىلى",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "473",
        iso6392: "ukr",
        iso6395: "ukr",
        iso6391: "uk",
        name: "Ukrainian",
        native: "українська мова",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "474",
        iso6392: "umb",
        iso6395: "umb",
        name: "Umbundu",
        native: "Úmbúndú",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "475",
        iso6392: "mis",
        iso6395: "mis",
        name: "Uncoded languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "476",
        iso6392: "und",
        iso6395: "und",
        name: "Undetermined",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "477",
        iso6392: "hsb",
        iso6395: "hsb",
        name: "Upper Sorbian",
        native: "hornjoserbšćina",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "478",
        iso6392: "urd",
        iso6395: "urd",
        iso6391: "ur",
        name: "Urdu",
        native: "اُردُو",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "479",
        iso6392: "uzb",
        iso6395: "uzb",
        iso6391: "uz",
        name: "Uzbek",
        native: "Oʻzbekcha / ўзбекча / ئوزبېچه  oʻzbek tili / ўзбек тили / ئوبېک تیلی",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "480",
        iso6392: "vai",
        iso6395: "vai",
        name: "Vai",
        native: "ꕙꔤ",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "481",
        iso6392: "ven",
        iso6395: "ven",
        iso6391: "ve",
        name: "Venda",
        native: "Tshivenḓa",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "482",
        iso6392: "vie",
        iso6395: "vie",
        iso6391: "vi",
        name: "Vietnamese",
        native: "Tiếng Việt",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "483",
        iso6392: "vol",
        iso6395: "vol",
        iso6391: "vo",
        name: "Volapük",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "484",
        iso6392: "vot",
        iso6395: "vot",
        name: "Votic",
        native: "vađđa ceeli",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "485",
        iso6392: "wak",
        name: "Wakashan languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "486",
        iso6392: "wln",
        iso6395: "wln",
        iso6391: "wa",
        name: "Walloon",
        native: "Walon",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "487",
        iso6392: "war",
        iso6395: "war",
        name: "Waray",
        native: "Winaray  Samareño  Lineyte-Samarnon  Binisayâ nga Winaray  Binisayâ nga Samar-Leyte  “Binisayâ nga Waray”",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "488",
        iso6392: "was",
        iso6395: "was",
        name: "Washo",
        native: "wá:šiw ʔítlu",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "489",
        iso6392: "cym / wel*",
        iso6395: "cym",
        iso6391: "cy",
        name: "Welsh",
        native: "Cymraeg  y Gymraeg",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "490",
        iso6392: "wel* / cym",
        iso6395: "cym",
        iso6391: "cy",
        name: "Welsh",
        native: "Cymraeg  y Gymraeg",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "491",
        iso6392: "fry",
        iso6395: "fry",
        iso6391: "fy",
        name: "Western Frisian",
        native: "Frysk",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "492",
        iso6392: "wal",
        iso6395: "wal",
        name: "Wolaitta  Wolaytta",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "493",
        iso6392: "wol",
        iso6395: "wol",
        iso6391: "wo",
        name: "Wolof",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "494",
        iso6392: "xho",
        iso6395: "xho",
        iso6391: "xh",
        name: "Xhosa",
        native: "isiXhosa",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "495",
        iso6392: "sah",
        iso6395: "sah",
        name: "Yakut",
        native: "Сахалыы",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "496",
        iso6392: "yao",
        iso6395: "yao",
        name: "Yao",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "497",
        iso6392: "yap",
        iso6395: "yap",
        name: "Yapese",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "498",
        iso6392: "yid",
        iso6395: "yid",
        iso6391: "yi",
        name: "Yiddish",
        native: "ייִדיש  יידיש  אידיש",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "499",
        iso6392: "yor",
        iso6395: "yor",
        iso6391: "yo",
        name: "Yoruba",
        native: "èdè Yorùbá",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "500",
        iso6392: "ypk",
        name: "Yupik languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "501",
        iso6392: "znd",
        name: "Zande languages",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "502",
        iso6392: "zap",
        iso6395: "zap",
        name: "Zapotec",
        native: "Diidxazá",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "503",
        iso6392: "zza",
        iso6395: "zza",
        name: "Zaza  Dimili  Dimli  Kirdki  Kirmanjki  Zazaki",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "504",
        iso6392: "zen",
        iso6395: "zen",
        name: "Zenaga",
        native: "Tuḍḍungiyya",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "505",
        iso6392: "zha",
        iso6395: "zha",
        iso6391: "za",
        name: "Zhuang  Chuang",
        native: "Vahcuengh / 話僮",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "506",
        iso6392: "zul",
        iso6395: "zul",
        iso6391: "zu",
        name: "Zulu",
        native: "isiZulu",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "507",
        iso6392: "zun",
        iso6395: "zun",
        name: "Zuni",
        native: "Shiwi'ma",
        f_active: "0",
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ])
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {}) 
     */
    return queryInterface.bulkDelete('languages', null, {})
  }
} 
