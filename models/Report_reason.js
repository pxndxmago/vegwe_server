module.exports = (db, DataTypes) => {
    const report_reason = db.define('report_reason', {
        idReportReason: { type: DataTypes.INTEGER, primaryKey: true, allowNull: false, autoIncrement: true  },
        reason: { type: DataTypes.STRING(150), allowNull: false }
    });
    return report_reason;
};