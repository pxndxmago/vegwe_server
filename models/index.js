'use strict';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const { DataTypes, Op } = require('sequelize');
const basename = path.basename(__filename);
const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/../config/config.json')[env];
const db = {};

let sequelize;

if (config.use_env_variable) {
  sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else {
  sequelize = new Sequelize(config.database, config.username, config.password, config);
}

fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    const model = sequelize['import'](path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});


//ASSOCIATIONS
// ************* social *************

db.chat.hasMany(db.user_chat, {
  foreignKey: 'idChat',
  onDelete: 'cascade'

});

db.user_chat.belongsTo(db.chat, {
  foreignKey: 'idChat',
  onDelete: 'cascade'

});

db.user.hasOne(db.user_chat, {
  foreignKey: 'idUser',
  onDelete: 'cascade'
  // sourceKey: 'idUser',

});

db.user_chat.belongsTo(db.user, {
  foreignKey: 'idUser',
  onDelete: 'cascade'

});


db.user.hasMany(db.image_user, {
  foreignKey: 'idUser',
  onDelete: 'cascade'
});

db.chat.hasMany(db.image_group, {
  foreignKey: 'idGroup',
  onDelete: 'cascade'
});

db.user.hasOne(db.like, {
  foreignKey: 'idUserLiked',
  onDelete: 'cascade'

});
db.like.belongsTo(db.user, {
  foreignKey: 'idUserLiked',
  onDelete: 'cascade'

});
db.user.hasOne(db.recipe_comment, {
  foreignKey: 'idUser',
  onDelete: 'cascade'
  // constraints:false
});

db.recipe.hasOne(db.recipe_comment, {
  foreignKey: 'idRecipe',
  onDelete: 'cascade'
  // constraints:false
});


// countries and locations
db.country.hasMany(db.city, {
  foreignKey: 'country_id',
  onDelete: 'cascade'
  // constraints:false
});

db.user_location.belongsTo(db.user, {
  foreignKey: 'idUser',
  onDelete: 'cascade'
  // constraints:false
});

db.user.hasMany(db.user_location, {
  foreignKey: 'idUser',
  onDelete: 'cascade'
  // constraints:false
});

db.chat.hasMany(db.chat_seen, {
  foreignKey: 'idChat',
  onDelete: 'cascade'
  // constraints:false
});

db.chat_seen.belongsTo(db.chat, {
  foreignKey: 'idChat',
  onDelete: 'cascade'
  // constraints:false
});

db.chat_seen.belongsTo(db.user, {
  foreignKey: 'idUser',
  onDelete: 'cascade'
});

db.message.belongsTo(db.user, {
  foreignKey: 'idSender',
  onDelete: 'cascade'
});

db.message.belongsTo(db.chat, {
  foreignKey: 'idChat',
  onDelete: 'cascade'
});

db.user.belongsTo(db.country, {
  foreignKey: 'idCountry',
});

db.user.belongsTo(db.city, {
  foreignKey: 'idCity',
});

db.report_reason.hasOne(db.report, {
  foreignKey: 'idReportReason',
  // sourceKey: 'idCity',
  // constraints:false
});

db.report.belongsTo(db.report_reason, {
  foreignKey: 'idReportReason',
});

// *************** RECIPES ****************

db.recipe.hasMany(db.step_recipe, {
  foreignKey: 'idRecipe'
});

db.recipe.hasMany(db.recipe_ingredient, {
  foreignKey: 'idRecipe'
});

db.recipe.hasMany(db.image_recipe, {
  foreignKey: 'idRecipe'
});

db.ingredient.hasOne(db.recipe_ingredient, {
  foreignKey: 'idIngredient',
});

db.user.hasMany(db.liked_recipe, {
  foreignKey: 'idUser'
});

db.recipe.hasMany(db.liked_recipe, {
  foreignKey: 'idRecipe'
});

db.liked_recipe.belongsTo(db.recipe, {
  foreignKey: "idRecipe"
});








db.sequelize = sequelize;
db.Sequelize = Sequelize;
db.op = Op;
module.exports = db;