const jwt = require('jsonwebtoken');
const config = require('../config/app_config.js');
const db = require('../models');
const { sequelize } = require("../models");

verifyToken = async (req, res, next) => {
    // console.log("verifing token");
    var response = Object();
    let token = req.headers['x-access-token'];
    // console.log("*******Token recibido******");
    // console.log(token);


    if (!token) {
        return res.send({
            status: 'fail', message: 'No token provided.'
        });
    }

    try {

        var decoded = jwt.verify(token, config.secret);
        var userExist = await db.user.findByPk(decoded.idUser);
        if (userExist) {
            req.idUser = decoded.idUser;
            next();
        } else {
            throw { message: "nouser" }
        }

    } catch (error) {
        console.log(error.message);
        response.status = 'fail';

        if (error.message == 'jwt expired')
            response.message = "tokenexpired";
        else if (error.message == 'nouser')
            response.message = "tokenexpired";
        else
            response.message = error.message;

        return res.send(response);
    }


}
const authJwt = {};

authJwt.verifyToken = verifyToken;

module.exports = authJwt;