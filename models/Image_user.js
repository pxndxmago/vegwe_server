module.exports = (db, DataTypes) => {
    const image_user = db.define('image_user', {
        idImage: { type: DataTypes.INTEGER, primaryKey: true, allowNull: false, autoIncrement: true },
        principal: { type: DataTypes.INTEGER, allowNull: false },
        route: { type: DataTypes.TEXT('long'), allowNull: false },
        thumbnailRoute: { type: DataTypes.TEXT('long'), allowNull: true },
        smallRoute: { type: DataTypes.TEXT('long'), allowNull: true },
        mediumRoute: { type: DataTypes.TEXT('long'), allowNull: true },
        largeRoute: { type: DataTypes.TEXT('long'), allowNull: true },
        f_active: { type: DataTypes.BOOLEAN, allowNull: false },
    });
    return image_user;
}