'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('report_reasons', [
      {
        reason: "Report spam",
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        reason: "Report scam",
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        reason: "Report inappropriate content",
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        reason: "Report rude or abusive",
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        reason: "Report user under 18",
        createdAt: new Date(),
        updatedAt: new Date()
      }, {
        reason: "Report stolen photos",
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('report_reasons', null, {});
  }
};
