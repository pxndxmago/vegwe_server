const express = require("express");
const config = require('../config/app_config.js');
const app = express();
const router = express.Router();
const db = require('../models');
const jwt = require('jsonwebtoken');
const CryptoJS = require("crypto-js");
const fs = require('fs');
const util = require("util");
const multer = require("multer");
const { Sequelize, Op } = require('sequelize');
const jwtAuth = require('../auth/verifyJwtToken');
const path = require('path');
const axios = require('axios')
const geolib = require('geolib');
const { sequelize } = require("../models");
const { nextTick } = require("process");
const sizeOf = require('image-size');
// const mkdirSync = util.promisify(fs.mkdirSync);
const sharp = require('sharp');

app.model = (model) => db[model];


const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        var directory;

        if (req.body.type == "user") {
            directory = "./api/resource/images/users/" + req.body.idOfType;
        }
        if (req.body.type == "group") {
            directory = "./api/resource/images/groups/" + req.body.idOfType;
        }
        if (req.body.type == "ingredient") {
            directory = "./api/resource/images/ingredient/";
        }
        if (req.body.type == "recipe") {
            directory = "./api/resource/images/recipe/" + req.body.title.replace(/\s/g, '');
        }
        if (fs.existsSync(directory)) {
            console.log("Directory exists.");
        } else {
            console.log("Directory no exists.");
            try {
                fs.mkdirSync(directory, { recursive: true });
            } catch (error) {
                console.log(error);
            }
        }
        console.log(JSON.stringify(file));

        cb(null, directory);
    },
    filename: function (req, file, cb) {
        var extension = file.originalname.split(".");
        cb(null, CryptoJS.SHA1(extension[0]) + "-" + Math.round((new Date()).getTime() / 1000) + '.' + extension[extension.length - 1]);
    }
});

const fileFilter = async (req, file, cb) => {
    const body = req.body;
    typeSelct = body.type == 'group' ? 'chat' : body.type;

    try {
        finder = await app.model(typeSelct).findByPk(body.idOfType);

        if (finder) {
            if (file.mimetype == "image/jpeg" || file.mimetype == "image/jpg" || file.mimetype == "image/png" || file.mimetype == "image/gif") {
                cb(null, true);
            } else {
                req.fileValidationError = 'Suba solo imagenes/gifs';
                return cb('Suba solo imagenes/gifs');
            }
        } else {
            return cb(typeSelct + ' don´t exist');
        }
    } catch (error) {

        return cb(error.message);
    }
}

const upload = multer({ storage: storage, fileFilter: fileFilter }).single('imageSend');

router.post("/signup", async (req, res) => {
    const response = new Object();
    var body = req.body;
    console.log("Notification token: " + body.notificationToken);

    const [user, created] = await db.user.findOrCreate({
        where: { email: body.email },
        defaults: {
            names: body.names,
            surnames: body.surnames,
            bornDate: body.bornDate,
            gender: body.gender,
            interestedIn: body.interestedIn,
            email: body.email,
            lookingFor: body.lookingFor,
            lifeStyle: body.lifeStyle,
            password: CryptoJS.SHA1(body.password).toString(),
            f_active: 1,
            nickName: body.email.split("@")[0],
            notificationToken: body.notificationToken
        }
    });

    if (created) {
        response.status = 'success';
        response.message = "user created";
        response.api_token = jwt.sign({ idUser: user.idUser }, config.secret, { expiresIn: config.expiresIn });
    } else {
        response.status = "fail";
        response.message = "existing user";
    }
    res.send(response);
});

router.post("/login", (req, res) => {
    const response = new Object();
    var passwordEncripted = CryptoJS.SHA1(req.body.password).toString();
    response.status = 'fail';
    db.user.findOne({
        where: {
            email: req.body.email,
        }
    }).then((user) => {
        if (user != null) {
            if (user.password == passwordEncripted) {
                response.status = 'success';
                response.api_token = jwt.sign({ idUser: user.idUser }, config.secret, { expiresIn: config.expiresIn });
                user.notificationToken = req.body.notificationToken;
                user.save();
                // response.user = user;
            } else {
                response.status = 'fail';
                response.error = 'password incorrect';
            }
        } else {
            response.status = 'fail';
            response.error = 'this user does not exist';
        }
        res.send(response);
    });
});

router.post("/loginWithFacebook", async (req, res) => {
    const response = new Object();
    // https://graph.facebook.com/v2.12/me?fields=name,picture,email&access_token=${token}
    var fbToken = req.headers['fb-token'];
    response.status = 'fail';
    try {
        const responseFb = await axios.get('https://graph.facebook.com/v2.12/me?fields=first_name,last_name,birthday,gender,picture,email&access_token=' + fbToken);
        if (responseFb.status == 200) {
            var user = await db.user.findOne({
                where: {
                    email: responseFb.data.email,
                }
            });
            if (user != null) {
                response.status = 'success';
                response.message = "Hi again " + user.names;
                response.api_token = jwt.sign({ idUser: user.idUser }, config.secret, { expiresIn: config.expiresIn });
            } else {

                const [user, created] = await db.user.findOrCreate({
                    where: { email: responseFb.data.email },
                    defaults: {
                        names: responseFb.data.first_name,
                        surnames: responseFb.data.last_name,
                        bornDate: (new Date(responseFb.data.birthday)).toISOString().split('T')[0],
                        gender: responseFb.data.gender,
                        email: responseFb.data.email,
                        lifeStyle: 'interested',
                        password: CryptoJS.SHA1(Math.random().toString(36).substr(2, 10)).toString(),
                        f_active: 1,
                        nickName: responseFb.data.email.split("@")[0],
                        notificationToken: req.body.notificationToken

                    }
                });

                if (created) {
                    response.status = 'success';
                    response.message = "welcome to vegwe";
                    response.api_token = jwt.sign({ idUser: user.idUser }, config.secret, { expiresIn: config.expiresIn });
                } else {
                    response.status = "fail";
                    response.message = "existing user";
                }

            }
            res.send(response);
        }
    } catch (error) {
        console.log(error);
        response.status = 'fail';
        response.error = 'Error fb';
        res.send(response);

    }

});

router.post("/loginWithGoogle", async (req, res) => {
    const response = new Object();
    // https://graph.facebook.com/v2.12/me?fields=name,picture,email&access_token=${token}
    var googleToken = req.headers['google-token'];
    response.status = 'fail';
    try {
        const responseGoogle = await axios.get('https://oauth2.googleapis.com/tokeninfo?access_token=' + googleToken);
        console.log(googleToken);

        // console.log(responseGoogle.data);

        if (responseGoogle.status == 200) {
            var user = await db.user.findOne({
                where: {
                    email: responseGoogle.data.email,
                }
            });
            if (user != null) {
                response.api_token = jwt.sign({ idUser: user.idUser }, config.secret, { expiresIn: config.expiresIn });
                user.notificationToken = req.body.notificationToken;
                await user.save();
                console.log("notification token");
                console.log(req.body.notificationToken);

                response.status = 'success';
                response.message = "Hi again " + user.names;
            } else {
                const responseGooglePeople = await axios.get('https://people.googleapis.com/v1/people/me?personFields=names,genders,birthdays',
                    { headers: { Authorization: "Bearer " + googleToken } });
                console.log(responseGooglePeople.data);

                var names = responseGooglePeople.data.names.length > 0 ? responseGooglePeople.data.names[0].givenName : '';
                var surnames = responseGooglePeople.data.names.length > 0 ? responseGooglePeople.data.names[0].familyName : '';
                var birthDay;
                for (const bd of responseGooglePeople.data.birthdays) {
                    console.log(JSON.stringify(bd));

                    if (bd.metadata.source.type == 'ACCOUNT') {
                        birthDay = bd;
                    }
                }
                var bornDate;
                if (birthDay != null) {
                    bornDate = (new Date(birthDay.date.year, birthDay.date.month - 1, birthDay.date.day)).toISOString().split('T')[0]
                } else {
                    bornDate = (new Date()).toISOString().split('T')[0]


                }
                console.log(responseGooglePeople);
                var gender = responseGooglePeople.data.genders != undefined && responseGooglePeople.data.genders.length > 0 ? responseGooglePeople.data.genders[0].value : '';
                const [user, created] = await db.user.findOrCreate({
                    where: { email: responseGoogle.data.email },
                    defaults: {
                        names: names,
                        surnames: surnames,
                        bornDate: bornDate,
                        gender: gender,
                        email: responseGoogle.data.email,
                        lifeStyle: 'interested',
                        password: CryptoJS.SHA1(Math.random().toString(36).substr(2, 10)).toString(),
                        f_active: 1,
                        nickName: responseGoogle.data.email.split("@")[0],
                        notificationToken: req.body.notificationToken
                    }
                });
                console.log("notificationToken");
                console.log(req.body.notificationToken);

                if (created) {
                    response.status = 'success';
                    response.message = "welcome to vegwe";
                    response.api_token = jwt.sign({ idUser: user.idUser }, config.secret, { expiresIn: config.expiresIn });
                } else {
                    response.status = "fail";
                    response.message = "existing user";
                }
                response.status = 'success';

            }
            res.send(response);
        }
    } catch (error) {
        console.log(error);
        response.status = 'fail';
        response.error = 'Error google';
        res.send(response);

    }

});

router.post('/checkUser', (req, res) => {
    const response = new Object();
    db.user.findOne({ where: { email: req.body.email } })
        .then((user) => {
            if (user != null) {
                response.status = "success";
                response.user = user;
                res.send(response);
            } else {
                response.status = "fail";
                // response.user = user;
                res.send(response);
            }
        });
});

router.post("/checkMail", (req, res) => {
    const response = new Object();
    var body = req.body;


    db.user.findAll({
        where: { email: body.email }
    }).then((err) => {
        if (err.length != 0) {
            response.status = "fail";
            response.description = "Usuario existente";
            res.send(response);
        }
        if (err.length == 0) {
            response.status = "success";
            response.description = "Usuario inexistente";
            res.send(response);
        }
    });
});

router.post("/changePassword", [jwtAuth.verifyToken], async (req, res) => {
    const response = new Object();
    var body = req.body;

    var user = await db.user.findByPk(req.idUser);
    if (body.newPassword == undefined || body.newPassword == '') {
        response.status = "fail";
        response.error = "incorrect new password";
        return res.send(response);
    }

    if (user) {
        if (user.password == CryptoJS.SHA1(body.password).toString()) {
            user.password = CryptoJS.SHA1(body.newPassword).toString();
            user.save();
            response.status = 'success';
            response.message = 'password updated';
        } else {
            response.status = "fail";
            response.error = "incorrect password";
        };
    }
    res.send(response);
})

router.post("/updateUserLocation", [jwtAuth.verifyToken], async (req, res) => {
    const response = new Object();
    var body = req.body;

    response.status = 'success';

    if (body.latitude != undefined && body.longitude != undefined) {
        try {
            var userLocation = db.user_location.create({
                latitude: body.latitude,
                longitude: body.longitude,
                idUser: req.idUser
            });
            if (userLocation) {
                console.log('Ubicacion registrada ' + body.latitude + " " + body.longitude);
            }

        } catch (error) {
            console.log(error);
            response.status = "fail";
            response.error = error;

            return res.send(response);
        }

    } else {
        response.status = "fail";
        response.error = error;
        return res.send(response);
    }

    var user = await db.user.findByPk(req.idUser);
    if (user) {
        if (body.country != undefined && body.country != "") {
            var countryMatch = await db.country.findOne({ where: { iso2: body.country.toLowerCase() } });
            if (countryMatch) {
                user.idCountry = countryMatch.id;
            }
        }
        if (body.city != undefined && body.city != "") {
            var cityMatch = await db.city.findOne({ where: { name: body.city.toLowerCase() } });
            if (cityMatch) {
                user.idCity = cityMatch.id;
            }
        }
        user.save();
    }

    res.send(response);
})
// <-------------------------------------- Users routes ----------------------------->

router.get('/users', [jwtAuth.verifyToken], async (req, res) => {
    const response = new Object();
    var options = {
        include: [{
            model: db.image_user,
            where: { f_active: { [Op.not]: 0 } },
            required: false,
            attributes: ['idImage', 'principal', 'idUser']
        }],
        where: {},
        order: [[db.image_user, 'principal', 'DESC'],]

    };
    if (req.query.limit != undefined && req.query.limit != '') {
        options.limit = parseInt(req.query.limit);
    }

    if (req.query.gender != undefined && req.query.gender != '') {
        var array = [];
        for (const item of req.query.gender.split(',')) {
            if (item != '')
                array.push(item);
        }
        if (array.length > 0) options.where.gender = array;
    }
    // Se filtra por pais recibido
    if (req.query.country != undefined && req.query.country != '') {
        if (req.query.country > 0) options.where.idCountry = req.query.country;
    }
    console.log(req.query.lifestyle);
    if (req.query.lifestyle != undefined && req.query.lifestyle != '') {
        var array = [];
        for (const item of req.query.lifestyle.split(',')) {
            if (item != '')
                array.push(item);
        }
        if (array.length > 0) options.where.lifestyle = array;
    }

    var notIn = [];

    if (req.query.except != undefined && req.query.except != '') {
        for (const item of req.query.except.split(',')) {
            if (item != '')
                notIn.push(item);
        }
    }

    var usersLiked = await db.like.findAll({ where: { idUser: req.idUser }, attributes: ['idUserLiked'] });

    notIn.push(req.idUser);
    for (const user of usersLiked) {
        notIn.push(user.idUserLiked);
    }

    // var usersBlocked = await db.like.findAll({
    //     where: {
    //         [Op.or]: [{ idUser: req.idUser },
    //         { idUserBlocked: req.idUser }],
    //     }, attributes: ['idUserLiked']
    // });

    options.where.idUser = { [Op.notIn]: notIn, }

    console.log("Cantidad de usuarios que no se mostraran: " + notIn.length);

    var users = await db.user.findAll(options);
    console.log("Cantidad de usuarios que se deberian mostrar: " + users.length);

    if (users != null) {
        response.status = "success";
        response.users = users;
        res.send(response);
    } else {
        response.status = "fail";
        // response.user = user;
        res.send(response);
    }

});

router.get('/users/:idUser', [jwtAuth.verifyToken], async (req, res) => {
    const response = new Object();
    response.status = "fail";

    if (req.params.idUser != undefined && req.params.idUser != '' && req.params.idUser > 0) {
        var user;
        try {
            user = await db.user.findOne({
                where: { idUser: req.params.idUser },
                attributes: [
                    'idUser',
                    'names',
                    'surnames',
                    'bornDate',
                    'email',
                    'nickName',
                    'password',
                    'f_active',
                    'location',
                    'description',
                    'interests',
                    'languages',
                    'lifeStyle',
                    'lookingFor',
                    'interestedIn',
                    'gender',
                    'job',
                    'notificationToken',
                ],
                // var image_groups = await db.image_group.findAll({
                //     where: { idGroup: chat.idChat, f_active: { [Op.not]: 0 } },
                //     attributes: ['idImage', 'principal', 'idGroup'],
                //     order: [['principal', 'DESC'],]
                // })
                include: [{
                    required: false,
                    model: db.image_user,
                    where: { f_active: { [Op.not]: 0 } },
                    attributes: ['idImage', 'principal', 'idUser'],

                }, {
                    required: false,
                    model: db.country,
                    // where: { f_active: { [Op.not]: 0 } },
                    attributes: ['id', 'name', 'emoji']
                }, {
                    required: false,
                    model: db.city,
                    // where: { f_active: { [Op.not]: 0 } },
                    attributes: ['id', 'name']
                }], order: [[db.image_user, 'principal', 'DESC'],]
            });
            // console.log(user);

        } catch (error) {
            console.log(error);
        }

        if (user != null) {

            // Se comprueba si el usuario actual puede obtener informacion de este usuario, si no, se envia un fail
            var isCurrentUserBlocked = await db.block.findOne({ where: { idUser: user.idUser, idUserBlocked: req.idUser } });

            if (isCurrentUserBlocked) {
                response.status = "fail";
                response.error = "user blocking";
                return res.send(response);
            }
            else {
                response.status = "success";
                response.user = user.get();
            }


            // Se comprueba si este usuario ha sido bloqueado por el usuario actual
            var isBlocked = await db.block.findOne({ where: { idUser: req.idUser, idUserBlocked: user.idUser } });

            if (isBlocked)
                response.user.isBlocked = true;
            else
                response.user.isBlocked = false;

            var isMatch = false;
            var userLike = await db.like.findOne({ where: { idUser: req.idUser, idUserLiked: user.idUser } });
            var userLiked = await db.like.findOne({ where: { idUser: user.idUser, idUserLiked: req.idUser } });

            if (userLike != null && userLiked != null)
                isMatch = true;
            else
                isMatch = false;

            response.user.isMatch = isMatch;

            try {
                var lastCurrentUserLocation = await db.user_location.findOne({ where: { idUser: req.idUser }, order: [['createdAt', 'ASC']], });
                var lastUserLocation = await db.user_location.findOne({ where: { idUser: user.idUser }, order: [['createdAt', 'ASC']], });

                if (lastUserLocation != null && lastCurrentUserLocation != null) {
                    console.log("Se intenta mostrar distance");
                    var distance = geolib.getDistance(
                        { latitude: lastCurrentUserLocation.latitude, longitude: lastCurrentUserLocation.longitude },
                        { latitude: lastUserLocation.latitude, longitude: lastUserLocation.longitude }
                    );
                    response.user.distance = distance;
                    console.log(distance);

                }
            } catch (error) {
                console.log(error);
            }
        } else {
            response.error = "No user associated to this id";
        }
    } else {
        response.error = "Incorrect idUser";
    }

    res.send(response);

});

router.put('/users', [jwtAuth.verifyToken], async (req, res) => {
    const response = new Object();
    const body = req.body;

    if (req.idUser != undefined && req.idUser != '' && req.idUser > 0) {
        var user = await db.user.findByPk(req.idUser);
        if (user != null) {
            response.status = "success";
            if (body.names != undefined) {
                user.names = body.names;
            }
            if (body.surnames != undefined) {
                user.surnames = body.surnames;
            }
            if (body.bornDate != undefined) {
                user.bornDate = body.bornDate;
            }
            // if (body.country != undefined) {
            //     user.idCountry = body.country;
            // }
            if (body.description != undefined) {
                user.description = body.description;
            }
            if (body.interests != undefined) {
                user.interests = body.interests;
            }
            if (body.languages != undefined) {
                user.languages = body.languages;
            }
            if (body.lifeStyle != undefined) {
                user.lifeStyle = body.lifeStyle;
            }
            if (body.lookingFor != undefined) {
                user.lookingFor = body.lookingFor;
            }
            if (body.interestedIn != undefined) {
                user.interestedIn = body.interestedIn;
            }
            if (body.gender != undefined) {
                user.gender = body.gender;
            }
            if (body.job != undefined) {
                user.job = body.job;
            }
            user.save();
            response.user = user;
        } else {
            response.status = "fail";
            errors = response.errors != undefined ? response.errors : [];
            errors.push("No user associated to this id");
            response.errors = errors;

        }
    } else {
        response.status = "fail";
        errors = response.errors != undefined ? response.errors : [];
        errors.push("Incorrect idUser");
        response.errors = errors;
    }
    res.send(response);

});

router.delete('/users', [jwtAuth.verifyToken], async (req, res) => {
    const response = new Object();
    const body = req.body;
    response.status = "fail";

    if (req.idUser != undefined && req.idUser != '' && req.idUser > 0) {

        var userExist = await db.user.findByPk(req.idUser);
        if (userExist == null)
            return res.send(response);
        var user_chats = await db.user_chat.findAll({
            include: [{
                model: db.chat,
                where: { f_group: 0 }
            }],
            where: { idUser: req.idUser },
        });

        var user_groups = await db.user_chat.findAll({
            include: [{
                model: db.chat,
                where: { f_group: 1 }
            }],
            where: { idUser: req.idUser },
        });

        const t = await sequelize.transaction();

        for (const chat of user_chats) {
            await db.user_chat.destroy({ where: { idChat: chat.idChat }, transaction: t }); //Se eliminan los usuarios de los chats
            await db.chat_seen.destroy({ where: { idChat: chat.idChat }, transaction: t }); //Se eliminan los vistos de mensajes
            await db.chat.destroy({ where: { idChat: chat.idChat }, transaction: t }); //Se eliminan los chats 
        }

        for (const group of user_groups) {
            var group_users = await db.user_chat.findAll({
                include: [{
                    model: db.chat,
                    where: { f_group: 1 }
                }],
                where: { idChat: group.idChat },
            });
            if (group_users.length < 2)
                await db.chat.destroy({ where: { idChat: group.idChat }, transaction: t }); //Se eliminan el grupo si solo queda 1 usuario

            await db.user_chat.destroy({ where: { idUser: req.idUser, idChat: group.idChat }, transaction: t }); //Se eliminan los usuarios de los chats
            await db.chat_seen.destroy({ where: { idChat: group.idChat }, transaction: t }); //Se eliminan los vistos de mensajes
            if (group_users.length > 1) {
                var hasAdmin = false;
                for (const user of group_users) {
                    if (user.idUser != req.idUser && user.isAdmin != null && user.isAdmin) {
                        hasAdmin = true;
                        break;
                    }
                }
                if (!hasAdmin) {
                    console.log("No hay admin");
                    for (const user of group_users) {
                        if (user.idUser != req.idUser) {
                            await db.user_chat.update({ isAdmin: 1 }, { where: { idUser: user.idUser, idChat: user.idChat }, t });
                            console.log("Se establece un nuevo admin");
                            break;
                        }
                    }
                }
            }
        }
        // await db.message.destroy({ where: { idUser: chat.idChat },transaction: t }); //Se eliminan 
        await db.image_user.destroy({ where: { idUser: req.idUser }, transaction: t }); //Se eliminan las imagenes del usuario
        await db.like.destroy({ where: { idUser: req.idUser }, transaction: t });//Se eliminan loslikes que dio
        await db.like.destroy({ where: { idUserLiked: req.idUser }, transaction: t });//Se eliminan los likes que recibio
        await db.report.destroy({ where: { idUser: req.idUser }, transaction: t });//Se eliminan los reportes hechos
        await db.report.destroy({ where: { idUserReported: req.idUser }, transaction: t });//Se eliminan los reportes recibidos
        await db.block.destroy({ where: { idUser: req.idUser }, transaction: t });//Se eliminan los bloqueos hechos
        await db.block.destroy({ where: { idUserBlocked: req.idUser }, transaction: t });//Se eliminan los bloqueos recibidos
        await db.user_location.destroy({ where: { idUser: req.idUser }, transaction: t });//Se eliminan las ubicaciones registradas 
        await db.user.destroy({ where: { idUser: req.idUser }, transaction: t });//Se eliminan el usuario
        var result = await t.commit();
        console.log(result);
        response.status = "success";
    } else {
        response.errors = errors;
    }
    res.send(response);

});

// <-------------------------------------- END Users routes ----------------------------->

// <-------------------------------------- CHAT routes ---------------------------------->
router.get('/languages', [jwtAuth.verifyToken], async (req, res) => {
    const response = new Object();
    var languages = await db.language.findAll({ where: { f_active: 1 }, attributes: ['id', 'iso6392', 'iso6395', 'iso6391', 'name', 'native'] });
    response.status = 'success';
    response.languages = languages;
    res.send(response);
});

router.get('/chats', [jwtAuth.verifyToken], async (req, res) => {
    const response = new Object();
    // Se obtienen los user_chat donde se encuentra el usuario
    var base_chats = await db.user_chat.findAll({
        include: [{
            model: db.chat,
            where: { f_group: 0 }
        }],
        attributes: ['idChat',
            [sequelize.literal(`(SELECT createdAt FROM messages m WHERE m.idChat = chat.idChat ORDER BY idMessage DESC LIMIT 1)`), 'lmCreatedAt']
        ],
        where: { idUser: req.idUser },
        order: [sequelize.literal('lmCreatedAt DESC')]
    });
    if (base_chats != null) {
        var chats = [];
        // Se recorren todos los idChat a los que pertenece y se buscan y listan los usuarios de cada uno
        // Se guarda en un array la informacion del chat y los usuarios que pertenecen a cada uno
        for (const chat of base_chats) {
            var ch = chat.get();
            if (ch.lmCreatedAt == null) {
                continue;
            }
            var new_chat = Object();
            new_chat.id = chat.idChat;
            new_chat.users = [];
            var usersByChat = await db.user_chat.findAll({
                include: [{
                    model: db.user,
                    include: [{
                        model: db.image_user,
                        where: { f_active: { [Op.not]: 0 } },
                        required: false,
                        attributes: ['idImage', 'principal']
                    }]
                }],
                attributes: ['idUser'],
                where: { idChat: chat.idChat }
            });
            // Si la cantidad de usuarios en el chat es 1 no se envia
            if (usersByChat != null && usersByChat.length < 2)
                continue;

            var chat_seen;
            try {
                chat_seen = await db.chat_seen.findOne({ where: { idChat: chat.idChat, idUser: req.idUser, f_seen: true } });
            } catch (error) {
                console.log(error);
            }
            if (chat_seen) {
                new_chat.seen = true;
            } else {
                new_chat.seen = false;
            }

            for (const user of usersByChat) {
                try {
                    var user_in_chat = Object();
                    user_in_chat.idUser = user.idUser;
                    user_in_chat.names = user.user.names;
                    user_in_chat.surnames = user.user.surnames;
                    user_in_chat.email = user.user.email;
                    user_in_chat.images = user.user.image_users;

                    new_chat.users.push(user_in_chat);
                } catch (error) {
                    console.log(error);
                }
            }


            var last_message = await db.message.findOne({ attributes: ['idMessage', 'idSender', 'message'], where: { idChat: chat.idChat }, order: [['createdAt', 'DESC']] });
            new_chat.lastMessage = last_message;
            // console.log(new_chat);
            chats.push(new_chat);
        }

        // console.log(chats);
        response.status = "success";
        response.chats = chats;

    } else {
        response.status = "fail";
    }
    res.send(response);
    // res.send(base_chats);
});

router.get('/chats/:idChat', [jwtAuth.verifyToken], async (req, res) => {
    const response = new Object();
    // console.log(req.idUser);

    const [chat_seen, created] = await db.chat_seen.findOrCreate({
        where: { idUser: req.idUser, idChat: req.params.idChat },
        defaults: { f_seen: true }
    });
    if (!created) {
        chat_seen.f_seen = 1;
        chat_seen.save();
    }

    var chat = await db.user_chat.findOne({
        include: [db.chat], attributes: ['idChat'],
        where: { idChat: req.params.idChat },
    });


    if (chat != null) {
        var new_chat = Object();
        new_chat.id = chat.idChat;
        new_chat.name = chat.chat.nameChat;
        new_chat.userBlocking = false;
        new_chat.isBlocked = false;
        new_chat.users = [];
        var usersByChat = await db.user_chat.findAll({
            include: [{
                model: db.user,
                include: [{
                    model: db.image_user,
                    where: { f_active: { [Op.not]: 0 } },
                    required: false,
                    attributes: ['idImage', 'principal']
                }],
                attributes: ['idUser', 'names', 'surnames', 'email']

            }],
            attributes: ['idUser'],
            where: { idChat: chat.idChat }
        });
        for (const user of usersByChat) {
            var user_in_chat = Object();
            user_in_chat.idUser = user.idUser;
            user_in_chat.names = user.user.names;
            user_in_chat.surnames = user.user.surnames;
            user_in_chat.email = user.user.email;
            user_in_chat.images = user.user.image_users;
            new_chat.users.push(user_in_chat);


            if (user.idUser != req.idUser) {
                // Se comprueba si el usuario actual esta bloqueado por el usuario, si existe un bloqueo se activa la bandera de userBlocking 
                // para que este ya no pueda enviar mensajes al chat
                var isCurrentUserBlocked = await db.block.findOne({ where: { idUser: user.idUser, idUserBlocked: req.idUser } });
                if (isCurrentUserBlocked)
                    new_chat.userBlocking = true;
                // Se comprueba si este usuario ha sido bloqueado por el usuario actual
                var isBlocked = await db.block.findOne({ where: { idUser: req.idUser, idUserBlocked: user.idUser } });
                if (isBlocked)
                    new_chat.isBlocked = true;
            }

        }
        var chat_seens;
        try {
            chat_seens = await db.chat_seen.findAll({ where: { idChat: chat.idChat, f_seen: true }, attributes: ['idUser', 'f_seen'] });
        } catch (error) {
            console.log(error);
        }
        if (chat_seens) {
            new_chat.seens = chat_seens;
        }
        response.status = "success";
        response.chat = new_chat;
    } else {
        response.status = "fail";
    }
    res.send(response);
    // res.send(base_chats);
});

router.post('/chats', [jwtAuth.verifyToken], async (req, res) => {
    console.log("Create chat");

    const response = new Object();
    // Se obtienen los user_chat donde se encuentra el usuario
    response.status = "fail";
    if (req.body.userChat == undefined ||
        req.body.userChat == "" ||
        req.body.userChat < 1) {
        response.error = "invalid userChat";
        return res.send(response);
    }

    var chat;
    // Se buscan todos los chats del usuario logueado
    var userChats = await db.user_chat.findAll({
        where: { idUser: req.idUser },
        attributes: ['idChat']
    });

    // Se recorren en busca de un chat ya existente

    if (userChats.length > 0) {
        for (const userChat of userChats) {
            var chatUsers = await db.user_chat.findAll({
                where: { idChat: userChat.idChat },
                attributes: ['idUser']
            });
            for (const user of chatUsers) {
                if (user.idUser == req.body.userChat) {
                    chat = await db.chat.findOne({ where: { idChat: userChat.idChat, f_group: 0, } });
                    break;
                }
            }
        }
    }

    if (!chat)
        chat = await db.chat.create({ nameChat: '', description: "", f_group: 0 });

    if (chat) {
        // Se agrega el usuario actual (remitente) y destinatario
        await db.user_chat.findOrCreate({
            where: { idChat: chat.idChat, idUser: req.idUser },
        });
        const [user_chat, userChatCreated] = await db.user_chat.findOrCreate({
            where: { idChat: chat.idChat, idUser: req.body.userChat },
        });

        var new_chat = Object();
        new_chat.id = chat.idChat;
        new_chat.name = chat.nameChat;
        new_chat.users = [];

        var usersByChat = await db.user_chat.findAll({
            include: [{
                model: db.user,
            }],
            attributes: ['idUser'],
            where: { idChat: chat.idChat }
        });

        for (const user of usersByChat) {
            var user_in_chat = Object();
            user_in_chat.idUser = user.idUser;
            user_in_chat.names = user.user.names;
            user_in_chat.surnames = user.user.surnames;
            user_in_chat.email = user.user.email;

            new_chat.users.push(user_in_chat);
        }

        var chat_seens;
        try {
            chat_seens = await db.chat_seen.findAll({ where: { idChat: chat.idChat, f_seen: true } });
        } catch (error) {
            console.log(error);
        }
        if (chat_seens) {
            new_chat.seens = chat_seens;
        }
        response.status = "success";
        response.chat = new_chat;
    }

    res.send(response);
});

router.delete('/chats/:idChat', [jwtAuth.verifyToken], async (req, res) => {
    const response = new Object();
    // Se obtienen los user_chat donde se encuentra el usuario
    response.status = "fail";
    if (req.params.idChat == undefined || req.params.idChat < 1) {
        response.error = "invalid idChat";
        return res.send(response);
    }
    // Se comprueba que el usuario exista en el chat
    var userExist = await db.user_chat.findOne({ where: { idUser: req.idUser, idChat: req.params.idChat } });

    if (userExist) {

        // Eliminar usuarios de un chat
        await db.user_chat.destroy({ where: { idChat: req.params.idChat } });

        // Eliminar mensajes de un chat
        // await db.messages.destroy({ where: { idChat: req.params.idChat } });

        // Eliminar chatSeens de un chat
        await db.chat_seen.destroy({ where: { idChat: req.params.idChat } });

        // Eliminar chatSeens de un chat
        await db.chat.destroy({ where: { idChat: req.params.idChat } });

        response.status = 'success';
    } else {
        response.error = "No chat associated with the given idChat";
        return res.send(response);
    }

    res.send(response);
});


router.get('/groups', [jwtAuth.verifyToken], async (req, res) => {
    const response = new Object();
    // Se obtienen los user_chat donde se encuentra el usuario
    var limit = 15;
    var offset = 0;
    var userChatWhere = {};
    var chatWhere = { f_group: 1 };

    if (req.query.limit != undefined && req.query.limit != '')
        limit = parseInt(req.query.limit);

    if (req.query.offset != undefined && req.query.offset != '')
        offset = parseInt(req.query.offset);

    var isPrivate = true;
    console.log(limit)
    console.log(offset)
    var base_chats = [];
    // Se verifica si es una busqueda de grupos privada o publica
    if (req.query.visibility != undefined && req.query.visibility == 'public') {
        isPrivate = false;

        if (req.query.nameChat != undefined && req.query.nameChat != '') {
            chatWhere.nameChat = { [Op.like]: '%' + req.query.nameChat + '%' };
        }

        if (req.query.countryId != undefined && req.query.countryId != '' && req.query.countryId > 0) {
            chatWhere.countryId = req.query.countryId;
        }

        var base_chats_temp = await db.user_chat.findAll({
            include: [{
                model: db.chat,
                where: chatWhere
            }],
            attributes: ['idChat'],
            group: ['chat.idChat'],
            where: userChatWhere, limit: limit,
            offset: offset,
            order: [['idChat']]

        });

        // Si se recibe el filtro languages se buscan los resultados que tengan lenguages del filtro
        // y se agregan, si no se recibe el filtro se envian todos los resultados

        if (req.query.languages != undefined && req.query.languages != '') {
            var languagesIds = req.query.languages.split(',');
            for (const ch of base_chats_temp) {
                for (const language of ch.chat.languages.split(',')) {
                    if (languagesIds.includes(language)) {
                        base_chats.push(ch);
                        break;
                    }
                }
            }
        } else {
            base_chats = base_chats_temp;
        }

    } else {
        var base_chats = await db.user_chat.findAll({
            include: [{
                model: db.chat,
                where: chatWhere,
            }],
            attributes: ['idChat'],
            where: { idUser: req.idUser },
            limit: limit,
            offset: offset,

        });

        console.log("Is private");
    }

    if (base_chats != null) {
        var chats = [];
        // Se recorren todos los idChat a los que pertenece y se buscan y listan los usuarios de cada uno
        // Se guarda en un array la informacion del chat y los usuarios que pertenecen a cada uno
        for (const chat of base_chats) {

            var new_chat = Object();
            new_chat.id = chat.idChat;
            new_chat.name = chat.chat.nameChat;
            new_chat.description = chat.chat.description;
            new_chat.countryId = chat.chat.countryId;
            new_chat.languages = chat.chat.languages;
            new_chat.isAdmin = false;
            new_chat.isMember = false;
            new_chat.users = [];

            var image_groups = await db.image_group.findAll({
                where: { idGroup: chat.idChat, f_active: { [Op.not]: 0 } },
                attributes: ['idImage', 'principal', 'idGroup'],
                order: [['principal', 'DESC'],]
            })
            new_chat.image_groups = image_groups != null ? image_groups : [];
            var usersByChat = await db.user_chat.findAll({
                include: [{
                    model: db.user,
                    include: [{
                        model: db.image_user,
                        where: { f_active: { [Op.not]: 0 } },
                        required: false,
                        attributes: ['idImage', 'principal']
                    }]
                }],
                attributes: ['idUser', 'isAdmin'],
                where: { idChat: chat.idChat },
                order: [[db.user, db.image_user, 'principal', 'DESC'],]
            });

            var chat_seens;
            try {
                chat_seens = await db.chat_seen.findAll({ where: { idChat: chat.idChat, f_seen: true } });
            } catch (error) {
                console.log(error);
            }
            if (chat_seens) {
                new_chat.seens = chat_seens;
            }

            for (const user of usersByChat) {
                try {
                    var user_in_chat = Object();
                    if (user.idUser == req.idUser) {
                        new_chat.isAdmin = user.isAdmin != null ? user.isAdmin : false;
                        new_chat.isMember = true;
                    }

                    user_in_chat.idUser = user.idUser;
                    user_in_chat.names = user.user.names;
                    user_in_chat.surnames = user.user.surnames;
                    user_in_chat.email = user.user.email;
                    user_in_chat.isAdmin = user.isAdmin != null ? user.isAdmin : false;
                    user_in_chat.images = user.user.image_users;


                    new_chat.users.push(user_in_chat);
                } catch (error) {
                    console.log(error);
                }
            }

            var last_message = await db.message.findOne({ attributes: ['idMessage', 'idSender', 'message'], where: { idChat: chat.idChat }, order: [['createdAt', 'DESC']] });
            new_chat.lastMessage = last_message;
            chats.push(new_chat);
        }

        // console.log(chats);
        response.status = "success";
        response.chats = chats;

    } else {
        response.status = "fail";
    }
    res.send(response);
    // res.send(base_chats);
});

router.post('/groups', [jwtAuth.verifyToken], async (req, res) => {
    const response = new Object();
    // Se obtienen los user_chat donde se encuentra el usuario
    response.status = "fail";

    if (req.body.nameChat == undefined || req.body.nameChat == "") {
        response.error = "invalid nameChat";
        return res.send(response);
    }
    if (req.body.description == undefined || req.body.description == "") {
        response.error = "invalid description";
        return res.send(response);
    }

    if (req.body.countryId == undefined || req.body.countryId == "") {
        response.error = "invalid countryId";
        return res.send(response);
    }

    if (req.body.languages == undefined || req.body.languages == "") {
        response.error = "invalid languages";
        return res.send(response);
    }

    var chat = await db.chat.findOne({ where: { nameChat: req.body.nameChat } });

    if (!chat) {
        chat = await db.chat.create({
            nameChat: req.body.nameChat,
            description: req.body.description,
            f_group: 1,
            countryId: req.body.countryId,
            languages: req.body.languages
        });
        await db.user_chat.findOrCreate({
            where: { idChat: chat.idChat, idUser: req.idUser, isAdmin: 1 },
        });

        response.status = "success";
        response.chat = chat;
    }

    res.send(response);
});


router.get('/groups/:idChat', [jwtAuth.verifyToken], async (req, res) => {
    const response = new Object();
    // console.log(req.idUser);

    const [chat_seen, created] = await db.chat_seen.findOrCreate({
        where: { idUser: req.idUser, idChat: req.params.idChat },
        defaults: { f_seen: true }
    });
    if (!created) {
        chat_seen.f_seen = 1;
        chat_seen.save();
    }

    var chat = await db.chat.findOne({
        where: {
            idChat: req.params.idChat, f_group: 1
        }, include: [{
            required: false,
            model: db.image_group,
            where: { f_active: { [Op.not]: 0 } },
            attributes: ['idImage', 'principal', 'idGroup']

        },], order: [[db.image_group, 'principal', 'DESC'],]
    });

    // var chat = await db.user_chat.findOne({
    //     include: [db.chat], attributes: ['idChat'],
    //     where: { idChat: req.params.idChat },
    // });


    if (chat != null) {
        var new_chat = Object();
        new_chat.id = chat.idChat;
        new_chat.name = chat.nameChat;
        new_chat.description = chat.description;
        new_chat.countryId = chat.countryId;
        new_chat.languages = chat.languages;
        new_chat.image_groups = chat.image_groups;
        new_chat.isAdmin = false;
        new_chat.isMember = false;
        new_chat.users = [];
        var usersByChat = await db.user_chat.findAll({
            include: [{
                model: db.user,
                include: [{
                    model: db.image_user,
                    where: { f_active: { [Op.not]: 0 } },
                    required: false,
                    attributes: ['idImage', 'principal']
                }],
                attributes: ['idUser', 'names', 'surnames', 'email', 'bornDate']
            }],
            attributes: ['idUser', 'isAdmin'],
            where: { idChat: chat.idChat },
            order: [[db.user, db.image_user, 'principal', 'DESC'],]
        });
        for (const user of usersByChat) {
            var user_in_chat = Object();
            if (user.idUser == req.idUser) {
                new_chat.isAdmin = user.isAdmin != null ? user.isAdmin : false
                new_chat.isMember = true;
            }
            user_in_chat.idUser = user.idUser;
            user_in_chat.names = user.user.names;
            user_in_chat.surnames = user.user.surnames;
            user_in_chat.email = user.user.email;
            user_in_chat.bornDate = user.user.bornDate;
            user_in_chat.isAdmin = user.isAdmin != null ? user.isAdmin : false;

            user_in_chat.images = user.user.image_users;

            new_chat.users.push(user_in_chat);
        }
        var chat_seens;
        try {
            chat_seens = await db.chat_seen.findAll({ where: { idChat: chat.idChat, f_seen: true }, attributes: ['idUser', 'f_seen'] });
        } catch (error) {
            console.log(error);
        }
        if (chat_seens) {
            new_chat.seens = chat_seens;
        }
        response.status = "success";
        response.chat = new_chat;
    } else {
        response.status = "fail";
    }
    res.send(response);
    // res.send(base_chats);
});

router.put('/groups/:idChat', [jwtAuth.verifyToken], async (req, res) => {
    const response = new Object();
    const body = req.body;

    if (body.nameChat == undefined || body.nameChat == "") {
        response.error = "invalid nameChat";
        return res.send(response);
    }
    if (body.description == undefined || body.description == "") {
        response.error = "invalid description";
        return res.send(response);
    }

    if (body.countryId == undefined || body.countryId == "") {
        response.error = "invalid countryId";
        return res.send(response);
    }

    if (body.languages == undefined || body.languages == "") {
        response.error = "invalid languages";
        return res.send(response);
    }

    if (req.params.idChat != undefined && req.params.idChat != '' && req.params.idChat > 0) {
        var group = await db.chat.findByPk(req.params.idChat);
        if (group != null) {
            response.status = "success";
            group.nameChat = body.nameChat;
            group.description = body.description;
            group.countryId = body.countryId;
            group.languages = body.languages;
            await group.save();
            response.group = group;
        } else {
            response.status = "fail";
            errors = response.errors != undefined ? response.errors : [];
            errors.push("No group associated to this id");
            response.errors = errors;

        }
    } else {
        response.status = "fail";
        errors = response.errors != undefined ? response.errors : [];
        errors.push("Incorrect idChat");
        response.errors = errors;
    }
    res.send(response);

});

router.post('/groups/join', [jwtAuth.verifyToken], async (req, res) => {
    const response = new Object();
    // console.log(req.idUser);
    response.status = 'fail';
    if (req.body.idChat == undefined || req.body.idChat < 1)
        return res.send(response);

    var userChat
    var chatExist = await db.chat.findByPk(req.body.idChat);
    if (chatExist)
        await sequelize.transaction(async (t) => {
            userChat = await db.user_chat.findOrCreate({ where: { idChat: req.body.idChat, idUser: req.idUser }, transaction: t });
        })
    if (userChat)
        response.status = 'success';
    res.send(response);
});

router.delete('/groups/:idChat', [jwtAuth.verifyToken], async (req, res) => {
    const response = new Object();
    // Se obtienen los user_chat donde se encuentra el usuario
    response.status = "fail";
    if (req.params.idChat == undefined || req.params.idChat < 1) {
        response.error = "invalid idChat";
        return res.send(response);
    }
    // Se comprueba que el usuario exista en el chat
    var userExist = await db.user_chat.findOne({ where: { idUser: req.idUser, idChat: req.params.idChat } });

    if (userExist.isAdmin != null && userExist.isAdmin) {

        // Eliminar usuarios de un chat
        await db.user_chat.destroy({ where: { idChat: req.params.idChat } });

        // Eliminar mensajes de un chat
        // await db.messages.destroy({ where: { idChat: req.params.idChat } });

        // Eliminar chatSeens de un chat
        await db.chat_seen.destroy({ where: { idChat: req.params.idChat } });

        // Eliminar chatSeens de un chat
        await db.chat.destroy({ where: { idChat: req.params.idChat } });

        response.status = 'success';
    } else {
        response.error = "No group associated with the given idChat";
        return res.send(response);
    }

    res.send(response);
});

router.get('/messages', [jwtAuth.verifyToken], async (req, res) => {
    const response = new Object();
    // Se obtienen los user_chat donde se encuentra el usuario

    var whereCondition = {}

    // Si el idChat recibido por el request no es nulo ni vacio se agrega condicion
    if (req.query.idChat != undefined && req.query.idChat != '' && req.query.idChat > 0) {
        whereCondition.idChat = req.query.idChat;
    }
    else {
        response.status = "fail";
        errors = response.errors != undefined ? response.errors : [];
        errors.push("Incorrect idChat");
        response.errors = errors;
    }

    // Si el idChat recibido por el request no es nulo ni vacio se agrega condicion
    if (req.query.idMessage != undefined && req.query.idMessage != '') {
        if (req.query.idMessage > 0) {
            try {
                whereCondition.idMessage = {
                    [db.op.lt]: parseInt(req.query.idMessage)
                };
            } catch (error) {
                console.log(error);

            }
        } else {
            response.status = "fail";
            errors = response.errors != undefined ? response.errors : [];
            errors.push("Incorrect idMessage");
            response.errors = errors;
        }
    }

    if (response.errors == undefined) {

        var user_chat = await db.user_chat.findAll({
            where: {
                idChat: req.query.idChat,
                idUser: req.idUser
            }
        });
        if (user_chat != null) {
            var last10Messages = await db.message.findAll({
                where: whereCondition,
                order: [['idMessage', 'DESC']],
                limit: req.query.limit != undefined ? parseInt(req.query.limit) : 10
            });
            if (last10Messages != null) {
                var messages = [];
                // // Se recorren todos los idChat a los que pertenece y se buscan y listan los usuarios de cada uno
                // // Se guarda en un array la informacion del chat y los usuarios que pertenecen a cada uno
                for (const message of last10Messages) {
                    var new_message = Object();
                    new_message.idMessage = message.idMessage;
                    new_message.idChat = message.idChat;
                    new_message.idSender = message.idSender;
                    var sender = await db.user.findOne({
                        where: { idUser: message.idSender },
                        include: [{
                            required: false,
                            model: db.image_user,
                            where: { f_active: { [Op.not]: 0 } },
                            attributes: ['idImage', 'principal', 'idUser']
                        }],
                        order: [[db.image_user, 'principal', 'DESC'],],
                        attributes: [
                            'idUser',
                            'names',
                            'surnames'
                        ]
                    });
                    if (sender)
                        new_message.sender = sender;
                    else
                        continue;
                    new_message.message = message.message;
                    new_message.createdAt = message.createdAt;
                    messages.push(new_message);
                }

                response.status = "success";
                response.messages = messages;
            }
        }
    }

    res.send(response);
});

router.post('/uploadImage', [jwtAuth.verifyToken], async (req, res) => {
    const response = new Object();
    await upload(req, res, async (err) => {
        if (err) {
            console.log("Error: " + JSON.stringify(err));

            response.status = 'fail';
            response.message = err;
            res.status(500).send(response)
        } else {
            try {
                var dimensions = sizeOf(req.file.path);

                var thumbnailPath = "./api/resource/images/" + req.body.type + "s/" + req.body.idOfType + "/thumbnail-" + req.file.filename;
                var smallPath = "./api/resource/images/" + req.body.type + "s/" + req.body.idOfType + "/small-" + req.file.filename;
                var mediumPath = "./api/resource/images/" + req.body.type + "s/" + req.body.idOfType + "/medium-" + req.file.filename;
                var largePath = "./api/resource/images/" + req.body.type + "s/" + req.body.idOfType + "/large-" + req.file.filename;

                var thumbnail = await sharp(req.file.path)
                    .resize(100)
                    .withMetadata()
                    .toFile(thumbnailPath);

                var small = await sharp(req.file.path)
                    .resize(300)
                    .withMetadata()
                    .toFile(smallPath);

                var medium = await sharp(req.file.path)
                    .resize(650)
                    .withMetadata()
                    .toFile(mediumPath);

                var large = await sharp(req.file.path)
                    .resize(1365)
                    .withMetadata()
                    .toFile(largePath);

                // console.log(thumbnail);
                var obj = {
                    type: req.body.type,
                    principal: req.body.principal,
                    route: req.file.path,
                    thumbnailRoute: thumbnailPath,
                    smallRoute: smallPath,
                    mediumRoute: mediumPath,
                    largeRoute: largePath,
                    // route: req.body.idOfType + "/" + req.file.filename
                };
                switch (req.body.type) {
                    case 'user':
                        if (req.body.principal == 1)
                            await db.image_user.update({ principal: 0 }, { where: { idUser: req.body.idOfType } })

                        obj.idUser = req.body.idOfType;
                        obj.f_active = 1;
                        break;
                    case 'group':

                        await db.image_group.update({ principal: 0 }, { where: { idGroup: req.body.idOfType } })

                        obj.idGroup = req.body.idOfType;
                        obj.principal = 1;
                        obj.f_active = 1;
                        break;
                    case 'recipe':
                        obj.idRecipe = req.body.idOfType;
                        break;
                    default:
                        break;
                }
                const image = await app.model("image_" + req.body.type).create(obj);
                if (image) {
                    response.status = 'success';
                    response.message = "Image Uploaded";
                    response.image = image;
                } else {
                    response.status = 'fail';
                    response.message = "Something happend";
                }
            } catch (error) {
                console.log(error);
                response.status = 'fail';
                response.message = error.message;
            }
            res.status(200).send(response);
        }
    });
});

router.delete('/userImages/:idImage', [jwtAuth.verifyToken], async (req, res) => {
    // console.log(path.dirname());
    console.log(req.params.idImage);
    const response = new Object();

    var user_image = await db.image_user.findByPk(req.params.idImage);

    if (user_image) {
        user_image.f_active = 0;
        user_image.save();
        response.status = 'success';
    } else {
        response.status = 'fail';
        res.status(500);
    }
    res.send(response);
});

router.post('/userImages/setImageProfile/:idImage', [jwtAuth.verifyToken], async (req, res) => {
    // console.log(path.dirname());
    const response = new Object();
    response.status = 'fail';
    await sequelize.transaction(async (t) => {
        await db.image_user.update({ principal: 0 }, { where: { idUser: req.idUser }, transaction: t });
        await db.image_user.update({ principal: 1 }, { where: { idImage: req.params.idImage }, transaction: t });
        response.status = 'success';
    });
    console.log(response);
    res.send(response);
});

router.get('/userImages/:idImage', async (req, res) => {
    // console.log(path.dirname());

    var user_image = await db.image_user.findOne({
        where: {
            idImage: req.params.idImage,
            f_active: { [Op.not]: 0 }
        },
    });

    if (user_image) {
        // express.static(path.resolve(__dirname, '../', user_image.route));
        var image;
        if (req.query.size == undefined) {
            image = path.resolve(__dirname, '../', user_image.route.trim());
        } else {
            switch (req.query.size) {
                case 'thumbnail':
                    image = path.resolve(__dirname, '../', user_image.thumbnailRoute.trim());
                    break;
                case 'small':
                    image = path.resolve(__dirname, '../', user_image.smallRoute.trim());
                    break;
                case 'medium':
                    image = path.resolve(__dirname, '../', user_image.mediumRoute.trim());
                    break;
                case 'large':
                    image = path.resolve(__dirname, '../', user_image.largeRoute.trim());
                    break;
            }
        }
        res.sendFile(image);
    } else {
        res.status(500).send({ status: 'fail' });
    }

    // res.sendFile(__dirname+'/api/resource/images/users/1/64d2e7225321f6cce1cd4b3b6d4ef5f0878743e7-1587519446.jpg');
});

router.get('/groupImages/:idImage', async (req, res) => {
    // console.log(path.dirname());

    var group_image = await db.image_group.findOne({
        where: {
            idImage: req.params.idImage,
            f_active: { [Op.not]: 0 }
        },
    });

    if (group_image) {
        // express.static(path.resolve(__dirname, '../', user_image.route));
        var image;
        if (req.query.size == undefined) {
            image = path.resolve(__dirname, '../', group_image.route.trim());
        } else {
            switch (req.query.size) {
                case 'thumbnail':
                    image = path.resolve(__dirname, '../', group_image.thumbnailRoute.trim());
                    break;
                case 'small':
                    image = path.resolve(__dirname, '../', group_image.smallRoute.trim());
                    break;
                case 'medium':
                    image = path.resolve(__dirname, '../', group_image.mediumRoute.trim());
                    break;
                case 'large':
                    image = path.resolve(__dirname, '../', group_image.largeRoute.trim());
                    break;
            }
        }
        res.sendFile(image);
    } else {
        res.status(500).send({ status: 'fail' });
    }

    // res.sendFile(__dirname+'/api/resource/images/users/1/64d2e7225321f6cce1cd4b3b6d4ef5f0878743e7-1587519446.jpg');
});

router.get('/userImages', [jwtAuth.verifyToken], async (req, res) => {
    const response = new Object();
    // Se obtienen los user_chat donde se encuentra el usuario

    var user_images = await db.image_user.findAll({
        where: {
            idUser: req.idUser, f_active: { [Op.not]: 0 },
        },
        attributes: ['idImage', 'principal', 'idUser']
    });
    if (user_images != null) {
        response.status = "success";
        response.images = user_images;
    } else {
        response.status = "fail";
    }
    res.send(response);

});

router.get('/interests/:locale', [jwtAuth.verifyToken], async (req, res) => {
    const response = Object();
    var defaultLocale = 'en_US';
    if (req.params.locale != undefined && req.params.locale != '') {
        defaultLocale = req.params.locale;
    }
    try {
        var interests = await db.interest.findAll({ where: { locale: defaultLocale } });
        response.status = 'success';
        response.locale = defaultLocale;
        response.interests = interests;
    } catch (error) {
        console.log(error);
        response.status = "fail";
        errors = response.errors != undefined ? response.errors : [];
        errors.push("Server error");
        response.errors = errors;
    }
    res.send(response);
});

router.post("/likes", [jwtAuth.verifyToken], async (req, res) => {
    const response = Object();
    var isMatch = false;
    try {
        if (req.body.idUserLiked != undefined && req.body.idUserLiked != "" && req.body.idUserLiked > 0) {
            var idUserLiked = parseInt(req.body.idUserLiked);
            var userLiked = await db.user.findByPk(idUserLiked);
            if (userLiked) {
                const [like, created] = await db.like.findOrCreate({
                    where: { idUser: req.idUser, idUserLiked: idUserLiked },
                    defaults: { f_liked: req.body.f_liked }
                });
                if (!created) {
                    like.f_liked = req.body.f_liked;
                    like.save();
                }
                var match = await db.like.findOne({ where: { idUserLiked: req.idUser, idUser: idUserLiked } });
                if (match && req.body.f_liked == 1) {
                    isMatch = true;
                }
                if (like) {
                    response.status = "success";
                    response.isMatch = isMatch;
                }
            } else {
                response.status = "fail";
                errors = response.errors != undefined ? response.errors : [];
                errors.push("idUserLiked does not exist");
                response.errors = errors;
            }
        } else {
            response.status = "fail";
            errors = response.errors != undefined ? response.errors : [];
            errors.push("Incorrect idUserLiked");
            response.errors = errors;

        }
    } catch (error) {
        console.log(error);

        response.status = "fail";
        errors = response.errors != undefined ? response.errors : [];
        errors.push("Server error");
        response.errors = errors;
    }
    res.send(response);

});

router.get("/likes", [jwtAuth.verifyToken], async (req, res) => {
    const response = Object();
    try {
        var likes = await db.like.findAll({
            where: {
                idUser: req.idUser,
                // [Op.not]: { 'f_liked': false },
            },
            order: [['updatedAt', 'DESC']],
            arguments: ['idLike', 'idUser', 'idUserLiked', 'f_liked'],
            include: [
                {
                    model: db.user, attributes: [
                        'names', 'bornDate',
                    ],
                    include: [{
                        model: db.image_user,
                        where: { f_active: { [Op.not]: 0 } },
                        required: false,
                        attributes: ['idImage', 'principal']
                    }]
                },
            ],
        });

        var likesMatch = [];
        for (const like of likes) {
            var newLike = like.get();
            var isMatch = false;
            if (newLike.f_liked != null && newLike.f_liked) {
                var temp = await db.like.findOne({ where: { idUser: newLike.idUserLiked, idUserLiked: req.idUser } });
                if (temp) isMatch = true;
            }
            newLike.isMatch = isMatch;
            likesMatch.push(newLike);
        }

        response.status = 'success';
        response.likes = likesMatch;
    } catch (error) {
        console.log(error);

        response.status = "fail";
        errors = response.errors != undefined ? response.errors : [];
        errors.push("Server error");
        response.errors = errors;
    }
    res.send(response);

});

router.get('/countries', async (req, res) => {
    const response = new Object();
    response.status = 'fail';
    var countries;
    try {
        countries = await db.country.findAll({
            attributes: ['id', 'name', 'iso3', 'native', 'emoji'],
            // include: {
            //     model: db.city, required: false,
            //     attributes: ['id', 'name', 'country_id', 'latitude', 'longitude'],
            // }
        });
        response.countries = countries;
        response.status = 'success';

    } catch (error) {
        console.log(error);
    }
    res.send(response);
});

router.get('/cities/:country', async (req, res) => {
    const response = new Object();
    response.status = 'fail';

    var cities;
    try {
        cities = await db.city.findAll({
            attributes: ['id', 'name', 'latitude', 'longitude'],
            where: { country_id: req.params.country }
        });
        response.cities = cities;
        response.status = 'success';

    } catch (error) {
        console.log(error);
    }

    res.send(response);
});

router.post('/blocks', [jwtAuth.verifyToken], async (req, res) => {
    const response = Object();
    response.status = 'fail';

    if (req.body.idUserBlocked == undefined || req.body.idUserBlocked == '') {
        response.error = "invalid idUserBlocked";
        return res.send(response);
    }

    if (req.body.block == undefined || parseInt(req.body.block) > 1 || parseInt(req.body.block) < 0) {
        response.error = "invalid block option";
        return res.send(response);
    }

    var block;
    if (req.body.block == 1) {
        block = await db.block.findOrCreate({
            where: { idUser: req.idUser, idUserBlocked: parseInt(req.body.idUserBlocked) }
        });
        response.blocked = true;
        response.status = 'success';
    }
    else if (req.body.block == 0) {
        block = await db.block.findOne({
            where: { idUser: req.idUser, idUserBlocked: parseInt(req.body.idUserBlocked) }
        });
        if (block) await block.destroy();
        response.blocked = false;
        response.status = 'success';
    }

    res.send(response);
})

router.get('/reportReasons', [jwtAuth.verifyToken], async (req, res) => {
    const response = new Object();
    response.status = 'fail';
    var reportReasons;
    try {
        reportReasons = await db.report_reason.findAll({
            attributes: ['idReportReason', 'reason'],
        });
        response.reportReasons = reportReasons;
        response.status = 'success';

    } catch (error) {
        console.log(error);
    }
    res.send(response);
});

router.post('/reports', [jwtAuth.verifyToken], async (req, res) => {
    const response = Object();
    response.status = 'fail';

    if (req.body.idUserReported == undefined || req.body.idUserReported == '') {
        response.error = "invalid idUserReported";
        return res.send(response);
    }

    if (req.body.idReportReason == undefined || parseInt(req.body.idReportReason) <= 0) {
        response.error = "invalid idReportReason";
        return res.send(response);
    }

    const [report, created] = await db.report.findOrCreate({
        attributes: ['idReport', 'idUser', 'idUserReported', 'idReportReason'],
        where: { idUser: req.idUser, idUserReported: parseInt(req.body.idUserReported) },
        defaults: { idReportReason: req.body.idReportReason }
    });

    response.report = report;
    response.status = 'success';

    res.send(response);
})

// <-------------------------------------- END CHAT routes ----------------------------->

// <-------------------------------------- RECIPES routes ---------------------------------->

const ingredientFilter = async (req, file, cb) => {
    // console.log(file);
    if (file.mimetype == "image/jpeg" || file.mimetype == "image/jpg" || file.mimetype == "image/png") {
        cb(null, true);
    } else {
        req.fileValidationError = 'Suba solo imagenes/gifs';
        return cb('Suba solo imagenes/gifs');
    }
}

const uploadIngredient = multer({ storage: storage, fileFilter: ingredientFilter }).single('imageSend');

router.post('/saveIngredient', async (req, res) => {
    const response = new Object();
    var dirImg;
    await uploadIngredient(req, res, async (err) => {
        if (typeof req.file !== "undefined") {
            response.message = "immage send";
            dirImg = "./api/resource/images/ingredient/" + req.file.path;
        } else {
            response.status = 'fail';
            response.message = err;
            dirImg = "./api/resource/images/ingredient/default.jpg";
        }
        const [ingredient, created] = await db.ingredient.findOrCreate({
            where: { name: req.body.name },
            defaults: {
                name: req.body.name,
                routeImage: dirImg
            }
        });
        if (created) {
            response.status = 'success';
            response.message = "ingredient created";
            response.ingredient = ingredient;
        } else {
            response.status = "fail";
            response.message = "existing ingredient";
        }
        res.send(response);
    });
});


const recipeFilter = async (req, file, cb) => {
    if (file.mimetype == "image/jpeg" || file.mimetype == "image/jpg" || file.mimetype == "image/png" || file.mimetype == "image/gif") {
        cb(null, true);
    } else {
        req.fileValidationError = 'Suba solo imagenes/gifs';
        return cb('Suba solo imagenes/gifs');
    }
}

const uploadRecipie = multer({ storage: storage, fileFilter: recipeFilter }).array('images', 6);

router.post("/generateRecipe", async (req, res) => {
    const response = new Object();
    var dirImg;
    await uploadRecipie(req, res, async (err) => {
        if (err) {
            response.status = "fail";
            response.message = err;
        } else {
            if (req.files.length != 0) {
                dirImg = req.files.map(file => {
                    var obj = {};
                    obj["principal"] = 1;
                    obj["route"] = file.path;
                    return obj
                });
            } else {
                dirImg = [{ "principal": 1, "route": "api/resource/images/recipe/default.jpg" }];
            }
            try {
                var existRecipe = await db.recipe.findOne({ where: { title: req.body.title } });
                if (existRecipe) {
                    response.status = "fail";
                    response.message = "existing recipe";
                } else {
                    const dataStep = JSON.parse(req.body.stepRecipe);
                    const dataIngredients = JSON.parse(req.body.recipeIngredient);
                    const recipe = await db.recipe.create({

                        title: req.body.title,
                        description: req.body.description,
                        likes: req.body.likes,
                        approximateTime: req.body.approximateTime,
                        difficulty: req.body.difficulty,
                        step_recipes: dataStep,
                        recipe_ingredients: dataIngredients,
                        image_recipes: dirImg,
                    }, {
                        include: [db.step_recipe, db.recipe_ingredient, db.image_recipe]
                    });
                    if (recipe) {
                        response.status = "success";
                        response.message = recipe;
                    } else {
                        response.status = "fail";
                        response.message = "error insertando";
                    }
                }
            } catch (error) {
                response.status = "fail";
                response.status = error.message;
            }
        }
        res.send(response);
    });
});

router.get("/getIdsLikedRecipe", async (req, res) => {
    const response = new Object();
    try {
        var getFavorites = await db.liked_recipe.findAll({
            attributes: ['idRecipe'],
            where: [{
                idUser: req.query.idUser
            }]
        });
        response.status = "success";
        response.message = getFavorites;
    } catch (error) {
        response.status = "fail";
        response.message = error.message;
    }
    res.send(response);
});

router.get("/getLikedRecipe", async (req, res) => {
    const response = new Object();
    try {
        var getFavorites = await db.liked_recipe.findAll({
            include: [
                { model: db.recipe, include: [{ model: db.image_recipe, where: { principal: 1 }, required: false, limit: 1 }] },
            ]
        });
        response.status = "success";
        response.message = getFavorites;
    } catch (error) {
        response.status = "fail";
        response.message = error.message;
    }
    res.send(response);
});

router.get("/getLastsRecipe", async (req, res) => {
    const response = new Object();
    try {
        var recipes = await db.recipe.findAll({
            // limit:5,
            order: [['idRecipe', 'DESC']],
            include: [{
                model: db.image_recipe,
                where: { principal: 1 }
            }]
        });
        if (recipes) {
            response.status = "success";
            response.message = recipes;
        } else {
            response.status = "fail";
            response.message = "not recipes";
        }
    } catch (error) {
        response.status = "fail";
        response.message = error.message;
    }
    res.send(response);
});

router.get("/getRandomRecipe", async (req, res) => {
    var idExisting;
    if (req.body.idExisting) {
        idExisting = JSON.parse(req.body.idExisting);
    } else {
        idExisting = []
    }

    var moreRandom = await db.recipe.findAll({
        limit: 10,
        order: Sequelize.literal('rand()'),
        where: {
            idRecipe: {
                [Op.notIn]: idExisting,
            }
        },
        include: [{
            model: db.image_recipe,
            where: { principal: 1 }
        }]
    });
    res.send(moreRandom);
});

router.get("/getRecipe", async (req, res) => {
    const response = new Object();
    var recipe = await db.recipe.findByPk(
        req.query.idRecipe,
        {
            order: [[db.step_recipe, 'stepNumber', 'ASC']],
            include: [
                { model: db.step_recipe },
                { model: db.recipe_ingredient, include: [{ model: db.ingredient }] },
                { model: db.image_recipe, where: [{ principal: 1 }] }
            ]
        }
    );
    if (recipe) {
        response.status = "success";
        response.message = recipe;
    } else {
        response.status = "fail";
        response.message = "404 recipe not found";
    }
    res.send(response);
});

router.post('/updateLikedRecipe', async (req, res) => {
    const response = new Object();
    console.log(req.body);

    if (req.body.actualState) {
        try {
            var destroyState = await db.liked_recipe.destroy({
                where: {
                    idRecipe: req.body.idRecipe,
                    idUser: req.body.idUser
                }
            });
            if (destroyState) {
                response.status = "success";
                response.message = "{'case':'destroy','idRecipe':" + req.body.idRecipe + "}";
            }
        } catch (error) {
            response.status = "fail";
            response.message = error.message;
        }
    } else {
        try {
            var [liked, created] = await db.liked_recipe.findOrCreate({
                where: {
                    idUser: req.body.idUser,
                    idRecipe: req.body.idRecipe
                },
                defaults: {
                    idUser: req.body.idUser,
                    idRecipe: req.body.idRecipe
                }
            });
            if (created) {
                response.status = "success";
                response.message = "{'case':'created','idRecipe':" + req.body.idRecipe + "}";
            } else {
                response.status = "success";
                response.message = "{'case':'existed','idRecipe':" + req.body.idRecipe + "}";
            }
        } catch (error) {
            response.status = "fail";
            response.message = error.message;
        }
    }
    console.log(response);

    res.send(response);
});

router.get('/getMyfavorites', async (req, res) => {
    const response = new Object();
    try {
        var getFavorites = await db.liked_recipe.findAll({
            where: {
                idUser: req.query.idUser
            },
            include: [
                { model: db.recipe, include: [{ model: db.image_recipe, where: { principal: 1 }, required: false }] },
            ]
        });
        response.status = "success";
        response.message = getFavorites;
    } catch (error) {
        response.status = "fail";
        response.message = error.message;
    }
    res.send(response);
});

router.get('/searchRecipe', async (req, res) => {
    const response = new Object();
    try {
        var findRecipe = await db.recipe.findAll({
            where: {
                title: {
                    [Op.like]: '%' + req.query.querySearch + '%'
                }
            },
            include: [{
                model: db.image_recipe,
                where: { principal: 1 }
            }]
        });
        response.status = "success";
        response.message = findRecipe;
    } catch (error) {
        response.status = "fail";
        response.message = error.message;
    }
    res.send(response);
});

router.post('/addCommentRecipe', async (req, res) => {
    const response = new Object();
    try {
        var [coment, created] = await db.recipe_comment.findOrCreate({
            where: {
                idUser: req.body.idUser,
                idRecipe: req.body.idRecipe
            },
            defaults: {
                idUser: req.body.idUser,
                idRecipe: req.body.idRecipe,
                assessment: req.body.assessment,
                commentary: req.body.commentary
            }
        });
        if (created) {
            response.status = "success";
            response.message = "Comentario añadido"
        } else {
            response.status = "fail";
            response.message = "Solo puedes dar una valoracion por receta";
        }
    } catch (error) {
        response.status = "fail";
        response.message = error.message;
    }
    console.log(response);
    res.send(response);

});

router.get('/getCommentRecipe', async (req, res) => {
    var comentaries = await db.recipe_comment.findAll({
        include: [{
            model: db.user
        }]
    });
    res.send(comentaries)
});
// <-------------------------------------- END RECIPES routes ----------------------------->
router.get('/testDistance', async (req, res) => {
    var response = new Object();
    response.distance = geolib.getDistance(
        { latitude: -2.116264, longitude: -79.885494 },
        { latitude: -2.126781, longitude: -79.914893 }
    );
    console.log(response.distance);

    res.send(response);
});

router.post('/testImageresize', async (req, res) => {
    await upload(req, res, async (err) => {
        if (err) {
            console.log("Error: " + JSON.stringify(err));

            response.status = 'fail';
            response.message = err;
            res.status(500).send(response)
        } else {
            try {
                var dimensions = sizeOf(req.file.path);
                var thumbnailPath = "./api/resource/images/groups/" + req.body.idOfType + "/thumbnail-" + req.file.filename;
                var smallPath = "./api/resource/images/groups/" + req.body.idOfType + "/small-" + req.file.filename;
                var mediumPath = "./api/resource/images/groups/" + req.body.idOfType + "/medium-" + req.file.filename;
                var largePath = "./api/resource/images/groups/" + req.body.idOfType + "/large" + req.file.filename;

                var thumbnail = await sharp(req.file.path)
                    .resize(50)
                    .withMetadata()
                    .toFile(thumbnailPath);

                var small = await sharp(req.file.path)
                    .resize(300)
                    .withMetadata()
                    .toFile(smallPath);

                var medium = await sharp(req.file.path)
                    .resize(650)
                    .withMetadata()
                    .toFile(mediumPath);

                var large = await sharp(req.file.path)
                    .resize(1365)
                    .withMetadata()
                    .toFile(largePath);

                console.log("width original de archivo: " + dimensions.width);
                console.log("height original de archivo: " + dimensions.height);
            } catch (error) {

            }
        }
    });
    return res.send({ ok: "OK" });
});

module.exports = router;
//////new recipes
////most voted
/////random