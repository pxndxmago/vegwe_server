const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const db = require("./models");
const { Sequelize, Op } = require('sequelize');
const config = require('./config/app_config.js');
const PORT = config.app_port
const PORTCHAT = config.chat_port
const server = require('http').createServer(app);
const io = require('socket.io').listen(server);
const jwt = require('jsonwebtoken');
const axios = require('axios')
const apiRoutes = require('./routes/api');
// const adminRouter = require('./routes/admin');
// app.use('/admin', adminRouter);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/api', apiRoutes);
app.use('/uimages', express.static(__dirname + '/api/resource/images/users'));

db.sequelize.sync();

app.listen(PORT, () => {
  console.log(`Escuchando el puerto *:${PORT}`);
});

server.listen(PORTCHAT, () => {
  console.log(`Escuchando el puerto de chat *:${PORTCHAT}`);
});

const nsp = io.of('/room');

const connections = {};

nsp.on('connection', (socket) => {
  var idUser;

  jwt.verify(socket.handshake.headers.token, config.secret, (err, decoded) => {
    if (err) {
      console.log(socket);
      console.log("TOKEN: " + socket.handshake.headers.token);

      console.log("Error jwt.verify en socket io connection: " + err);
    }
    try {
      idUser = decoded.idUser;
    } catch (error) {
      console.log(error);
    }
  });
  if (idUser != null)
    connections[idUser] = socket.id;
  else
    return false;

  console.log("Rooms:");
  console.log(connections);
  // console.log("Qery: " + JSON.stringify(socket.handshake.headers));

  socket.on('send message', async (message) => {

    var user;
    try {
      user = await db.user.findOne({
        where: { idUser: idUser },
        include: [{
          required: false,
          model: db.image_user,
          where: { f_active: { [Op.not]: 0 } },
          attributes: ['idImage', 'principal', 'idUser']
        }],
        order: [[db.image_user, 'principal', 'DESC'],],
        attributes: [
          'idUser',
          'names',
          'surnames'
        ]
      });

    } catch (error) {
      console.log("Error al buscar el usuario con idUser" + error);
    }
    var users_chat;

    if (user) {

      users_chat = await db.user_chat.findAll({
        include: [{ model: db.user }, {
          required: false,
          model: db.chat,
          attributes: ['nameChat', 'f_group']
        }
        ], where: { idChat: message.idChat }
      });

      // Si es un chat y no un grupo, se verifica que no hayan bloqueos antes de enviar un mensaje
      if (users_chat.length == 2 && !users_chat[0].chat.f_group) {
        console.log("Hola");
        for (const user of users_chat) {
          var isCurrentUserBlocked;
          var isBlocked;

          if (user.idUser != idUser) {
            isCurrentUserBlocked = await db.block.findOne({ where: { idUser: user.idUser, idUserBlocked: idUser } });
            isBlocked = await db.block.findOne({ where: { idUser: idUser, idUserBlocked: user.idUser } });
          }

          console.log("user.idUser: " + user.idUser);
          console.log("idUser: " + idUser);

          if (isCurrentUserBlocked != null || isBlocked != null) {
            console.log("Blocked user message not sended");
            return;
          }

        }
      }

    }
    // console.log(user);
    // se guarda en base el mensaje enviado
    var saved_message;

    try {
      saved_message = await db.message.create({
        idChat: message.idChat,
        idSender: idUser,
        message: message.message,
        f_active: 1
      });
    } catch (error) {
      console.log("Error al guardar el mensaje en bd" + error);
    }


    //se buscan los usuarios que pertenecen al chat y se envia un mensaje a cada uno de ellos 
    if (saved_message != null) {
      try {

        // console.log(users_chat);
        var dt = new Date();
        for (const user_chat of users_chat) {
          var room = connections[user_chat.idUser];

          var messageToSend = new Object();
          messageToSend.idMessage = Number(saved_message.idMessage);
          messageToSend.idChat = Number(saved_message.idChat);
          messageToSend.idSender = Number(saved_message.idSender);
          messageToSend.sender = user;
          messageToSend.message = saved_message.message;
          messageToSend.createdAt = saved_message.createdAt;
          // Se emite el mensaje al room del usuario conectado a la sesion de socketIO
          nsp.to(room).emit('new_message', messageToSend);
          if (user_chat.user.idUser != saved_message.idSender) {
            try {
              chatName = user_chat.user.names;
              if (user_chat.chat.f_group != null && user_chat.chat.f_group) {
                chatName = chatName + " - " + user_chat.chat.nameChat;
              }
              const options = {
                method: 'POST',
                headers: {
                  'Authorization': 'key=AAAAb56FzFE:APA91bGxY5UTVrG_QBUFX0Q48Xob7I2Y7dDNctW0ELclUxEXkHZh15ytFTn14VMv3oY6vViEy_GFfk2wBdt-NgjR0_-8_37Yj4npKWchVaO_dwQmnZo6lJ7ee7GbCbdKA6H9O2vqNRmY',
                  'Content-Type': 'application/json'
                },
                data: {
                  to: user_chat.user.notificationToken,
                  notification: {
                    title: chatName,
                    body: saved_message.message
                  },
                  data: {
                    "click_action": "FLUTTER_NOTIFICATION_CLICK",
                    "idChat": saved_message.idChat,
                    "isGroup": user_chat.chat.f_group != null && user_chat.chat.f_group ? 1 : 0
                  }
                },
                url: 'https://fcm.googleapis.com/fcm/send'
              }
              // console.log(options);

              // Se niegan las lecturas de chat de usuarios que no esten en el chat leyendo los mensajes
              if (room == null || room == '') {
                var seen = await db.chat_seen.findOne({
                  where: { idUser: user_chat.idUser, idChat: saved_message.idChat }
                });
                seen.f_seen = false;
                await seen.save();
              }

              // Se envias notificaciones push
              axios(options).then(function (response) {
                // console.log(response.data);
              }).catch(function (error) {
                console.log("No se pudo enviar notificacion");
              });

              console.log("Se envia mensaje a usuario: " + user_chat.user.names + " con id: " + user_chat.user.idUser);
            } catch (error) {
              console.log(error);
            }
          }


        }

      } catch (error) {
        console.log("Error" + error);

      }

    }

  });

  socket.on('disconnect', (reason) => {
    delete connections[idUser];
    console.log(reason);
    console.log(connections);

  });

});




