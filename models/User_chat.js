module.exports = (db, DataTypes) => {
    const user_chat = db.define('user_chat', {

        // idUser: { type: DataTypes.INTEGER, allowNull: false },
        // idChat: { type: DataTypes.INTEGER, allowNull: false }

        // idUser:{
        //     type: DataTypes.INTEGER,
        //     references:{
        //         model:'users',
        //         key:'idUser'
        //     },
        // },
        // idChat:{
        //     type:DataTypes.INTEGER,
        //     references:{
        //         model:'chats',
        //         key:'idChat'
        //     },
        // }
        isAdmin: { type: DataTypes.BOOLEAN, allowNull: true }
    }, {
        indexes: [
            {
                name: 'user_chats_id_user_id_chat',
                unique: true,
                fields: ['idUser', 'idChat']
            }
        ]
    });
    return user_chat;
};