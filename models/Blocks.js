module.exports = (db, DataTypes) => {
    const block = db.define('block', {
        idUser: { type: DataTypes.INTEGER, allowNull: false },
        idUserBlocked: { type: DataTypes.INTEGER, allowNull: false }
    }, {
        indexes: [
            {
                unique: true,
                fields: ['idUser', 'idUserBlocked']
            }
        ]
    });
    return block;
};