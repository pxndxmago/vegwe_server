'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    return Promise.all([
      queryInterface.addColumn('chats', 'description', Sequelize.STRING(1000), { allowNull: false }),
      queryInterface.addColumn('chats', 'countryId', Sequelize.INTEGER, { allowNull: true }),
      queryInterface.addColumn('chats', 'languages', Sequelize.STRING(500), { allowNull: true }),
      queryInterface.addColumn('chats', 'f_group', Sequelize.BOOLEAN, { allowNull: false }),
    ])

  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    return Promise.all([
      queryInterface.removeColumn('chats', 'description'),
      queryInterface.removeColumn('chats', 'countryId'),
      queryInterface.removeColumn('chats', 'languages'),
      queryInterface.removeColumn('chats', 'f_group'),
    ])
  }
};
